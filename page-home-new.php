<?php
/*
 * Template name: Home_Events_Mod
 */

get_header(); ?>
<div class="cf"></div>
<section class="page cf">
    <div class="iconssection cf">
        <div class="wrapper cf">
            <div class="iconbox iconone">
                <div class="imageboxicon" style="background: url(<?php echo ale_get_option('iconboxicon1'); ?>) 50% 50% no-repeat;"></div>
                <div class="titleboxicon">
                    <?php echo ale_get_option('iconboxtit1'); ?>
                </div>
                <div class="descboxicon">
                    <?php echo ale_get_option('iconboxdesc1'); ?>
                </div>
                <div class="linkboxicon">
                    <a href="<?php echo ale_get_option('iconboxlink1'); ?>" class="button"><?php _e('Read more','aletheme'); ?></a>
                </div>
            </div>
            <div class="iconbox icontwo">
                <div class="imageboxicon" style="background: url(<?php echo ale_get_option('iconboxicon2'); ?>) 50% 50% no-repeat;"></div>
                <div class="titleboxicon">
                    <?php echo ale_get_option('iconboxtit2'); ?>
                </div>
                <div class="descboxicon">
                    <?php echo ale_get_option('iconboxdesc2'); ?>
                </div>
                <div class="linkboxicon">
                    <a href="<?php echo ale_get_option('iconboxlink2'); ?>" class="button"><?php _e('Read more','aletheme'); ?></a>
                </div>
            </div>
            <div class="iconbox iconthree">
                <div class="imageboxicon" style="background: url(<?php echo ale_get_option('iconboxicon3'); ?>) 50% 50% no-repeat;"></div>
                <div class="titleboxicon">
                    <?php echo ale_get_option('iconboxtit3'); ?>
                </div>
                <div class="descboxicon">
                    <?php echo ale_get_option('iconboxdesc3'); ?>
                </div>
                <div class="linkboxicon">
                    <a href="<?php echo ale_get_option('iconboxlink3'); ?>" class="button"><?php _e('Read more','aletheme'); ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="cf"></div>
    <div class="abouthome cf" style="background: url('<?php echo ale_get_option('aboutboximg'); ?>') center no-repeat; background-size: cover;">
        <div class="blackbg cf">
            <div class="wrapper cf">
                <div class="abouttitle">
                    <?php echo ale_get_option('aboutboxtit'); ?>
                </div>
                <div class="aboutdescr">
                    <?php echo ale_get_option('aboutboxdesc'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="reviews revcolor cf">
        <div class="wrapper">
            <div class="revtit"><?php echo ale_get_option('revtit'); ?></div>
            <div class="revitems">
                <div class="itemrev">
                    <div class="leftimage">
                        <img src="<?php echo ale_get_option('revimage1') ?>" alt="<?php echo ale_get_option('revname1'); ?>" />
                    </div>
                    <div class="rightinfo">
                        <div class="titlere"><?php echo ale_get_option('revname1'); ?></div>
                        <div class="descre"><?php echo ale_get_option('revdesc1') ?></div>
                    </div>
                </div>
                <div class="itemrev">
                    <div class="leftimage">
                        <img src="<?php echo ale_get_option('revimage2') ?>" alt="<?php echo ale_get_option('revname2'); ?>" />
                    </div>
                    <div class="rightinfo">
                        <div class="titlere"><?php echo ale_get_option('revname2'); ?></div>
                        <div class="descre"><?php echo ale_get_option('revdesc2') ?></div>
                    </div>
                </div>
                <div class="itemrev">
                    <div class="leftimage">
                        <img src="<?php echo ale_get_option('revimage3') ?>" alt="<?php echo ale_get_option('revname3'); ?>" />
                    </div>
                    <div class="rightinfo">
                        <div class="titlere"><?php echo ale_get_option('revname3'); ?></div>
                        <div class="descre"><?php echo ale_get_option('revdesc3') ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cf"></div>
    <div class="bottomtwoboxes cf">
        <div class="wrapper cf">
            <div class="leftmenubox">
                <div class="ourmenubox cf">
                    <div class="titlebox cf">
                        <h2><?php _e('Our menu','aletheme'); ?></h2><ul class="custom-direction-nav"></ul>
                    </div>
                    <div class="menuitemslist cf">
                        <div class="menuslide">
                            <ul class="slides">
                        <?php query_posts('post_type=menu&posts_per_page=6');
                        if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <li class="menufooditem">
                                <div class="boxmenuitem cf">
                                    <div class="menuimage">
                                        <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'menu-home'); ?></a>
                                        <div class="mask">
                                            <a href="<?php the_permalink(); ?>">
                                                <span>+</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="whitebox cf">
                                        <div class="titile">
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </div>
                                        <div class="descr">
                                            <?php echo ale_truncate(get_the_excerpt(),70); ?>
                                        </div>
                                        <div class="menubottom">
                                            <div class="menulink fl">
                                                <a href="<?php the_permalink(); ?>" class="button redbutcolor">Read more</a>
                                            </div>
                                            <div class="pricemenu fl">
                                                <?php echo ale_get_meta('itemcost'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endwhile; endif; wp_reset_query(); ?>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div>
	<?php
	query_posts('category_name=Events&eventDisplay=upcoming');
	$calender = get_cat_ID( 'Events' ); $calender_link = get_category_link( $calender );
	$the_query = new wp_query('meta_key=_EventStartDate&meta_compare=>=&meta_value='.$todaysDate.'&orderby=meta_value&order=ASC');

	while ($the_query->have_posts()) : $the_query->the_post();

?> 

		<li>
			<?php
				$category = get_the_category();
				$categoryname = $category[1]->cat_name;
			?>
			<?php the_title('<div class="event-title"><a href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '" class="' .$categoryname . ' tectip">', '</a></div>'); ?>
			<div class="event-date">
				<?php
					echo the_event_start_date();
				?>
			</div>
				<?php
					echo substr($post->post_content, 0, 254);
					echo '<p><a href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '" class="' .$categoryname . ' tectip">... Read More About This Event</a></p>';
				?>
		</li>
<?php
	$alt = ( empty( $alt ) ) ? ' alt' : '';
	endwhile; // posts
?>
</section>
<?php get_footer(); ?>