<?php get_header(); ?>
    <section class="page menupagesection cf">
        <div class="wrapper">
            <?php get_breadcrumbs(); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="pagetitle">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="menutypelist cf menuitemsbottom">
                <div class="bigimagebox cf">
                    <div class="menuslider">
                        <ul class="slides">
                            <?php
                            $args = array(
                                'post_type' => 'attachment',
                                'numberposts' => -1,
                                'post_status' => null,
                                'order'				=> 'ASC',
                                'orderby'			=> 'menu_order ID',
                                'meta_query'		=> array(
                                    array(
                                        'key'		=> '_ale_hide_from_gallery',
                                        'value'		=> 0,
                                        'type'		=> 'DECIMAL',
                                    ),
                                ),
                                'post_parent' => $post->ID
                            );
                            $attachments = get_posts( $args );
                            if ( $attachments ) {
                                foreach ( $attachments as $attachment ) {
                                    echo "<li>".wp_get_attachment_image( $attachment->ID, 'menu-fullimage' )."</li>";
                                }
                            }
                            ?>
                        </ul>
                        <div class="bottommenuslider">
                            
                                                        <div class="itemcost">
                                <div class="costtext">
                                    <?php echo ale_get_meta('itemcost'); ?> <?php if(ale_get_option('currencyname')){ echo ale_get_option('currencyname'); } else { echo _e('dollars','aletheme'); } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="maskbox"></div>
                </div>
            </div>
            <div class="descriptionmenusection cf">
                <div class="leftpartmenu">
                    <div class="descic">
                        <h2><?php _e('Description',''); ?></h2>
                    </div>
                    <div class="text">
                        <?php the_content(); ?>
                    </div>
                </div>
             </div>
                <?php
                $querycategory = '';
                $taxyterm = get_the_terms($post->ID, 'menu-category');
                    foreach($taxyterm as $curcat){
                        $querycategory = $curcat->name;
                    }
                ?>
            <?php endwhile; else: ?>
                <?php ale_part('notfound')?>
            <?php endif; ?>
            <div class="cf"></div>
            <div class="recomitems">
                <h3><?php _e('Recommended dishes','aletheme'); ?></h3>
            </div>
            <div class="menutypelist cf">
                <?php wp_reset_query(); ?>
                <?php query_posts('&post_type=menu&orderby=rand&posts_per_page=3&menu-category='. $querycategory ); ?>

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="menutypeitem">
                        <div class="menuimage">
                            <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'menu-grid'); ?></a>
                            <div class="mask">
                                <a href="<?php the_permalink(); ?>">
                                    <span>+</span>
                                </a>
                            </div>
                        </div>
                        <div class="whitebox cf">
                            <div class="titile">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </div>
                            <div class="descr">
                                <?php echo ale_truncate(get_the_excerpt(),200); ?>
                            </div>
                            <div class="menubottom cf">
                                <div class="menulink fl">
                                    <a href="<?php the_permalink(); ?>" class="button redbutcolor">Read more</a>
                                </div>
                                <div class="pricemenu fr">
                                    <?php echo ale_get_meta('itemcost'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; else: ?>
                    <?php ale_part('notfound')?>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>