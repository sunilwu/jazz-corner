<?php get_header(); global $query_string; query_posts($query_string.'&posts_per_page=-1'); ?>
    <section class="page menupagesection cf">
        <div class="wrapper">
            <?php get_breadcrumbs(); ?>
            <div class="pagetitle gallytypetitle">
                <h2><?php _e('Gallery',''); ?></h2>
            </div>
            <div class="menutypelist gallerygrid cf">
                <div class="galleryfilterbox cf">
                    <ul id="filters" class="cf">
                        <li class="cf"><a href="#" class="active fil" data-filter="*"><?php _e('Show all', 'aletheme')?></a></li>
                        <?php $args = array(
                            'type'                     => 'gallery',
                            'child_of'                 => 0,
                            'parent'                   => '',
                            'orderby'                  => 'name',
                            'order'                    => 'ASC',
                            'hide_empty'               => 1,
                            'hierarchical'             => 1,
                            'exclude'                  => '',
                            'include'                  => '',
                            'number'                   => '',
                            'taxonomy'                 => 'gallery-category',
                            'pad_counts'               => false );

                        $categories = get_categories( $args );

                        foreach($categories as $cat){
                            echo '<li class="cf"><a href="#" class="fil" data-filter=".'.$cat->slug.'">'.$cat->name.'</a></li>';
                        }
                        ?>
                    </ul>
                </div>
                <div id="post" class="galleryitems cf">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="post galitem element <?php $terms = get_the_terms($post->id, 'gallery-category'); foreach($terms as $itcat) { echo $itcat->slug.' ';} ?>">
                            <div class="imagegally">
                                <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'gallery-tumba'); ?></a>
                                <div class="mask2">
                                    <div class="gallytitle"><a href="<?php the_permalink(); ?>"><?php echo ale_truncate(get_the_title(),20); ?></a></div>
                                    <div class="gallycat">
                                        <?php
                                        _e('Category: ','aletheme');
                                        $current_category = wp_get_post_terms($post->ID, 'gallery-category', array("fields" => "all"));
                                        if($current_category){
                                            foreach($current_category as $curcat){
                                                echo $curcat->name.' ';
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="gallydate bodyfont">
                                        <?php echo get_the_date(); ?>
                                    </div>
                                </div>
                                <div class="mask0"><a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'gallery-tumba'); ?></a></div>
                                <div class="countgal"><span>
                                    <?php
                                    $args = array(
                                        'post_type' => 'attachment',
                                        'numberposts' => -1,
                                        'post_status' => null,
                                        'order'				=> 'ASC',
                                        'orderby'			=> 'menu_order ID',
                                        'meta_query'		=> array(
                                            array(
                                                'key'		=> '_ale_hide_from_gallery',
                                                'value'		=> 0,
                                                'type'		=> 'DECIMAL',
                                            ),
                                        ),
                                        'post_parent' => $post->ID
                                    );
                                    $attachments = get_posts( $args );

                                    echo count($attachments); ?>
                                    <?php _e('Photos','aletheme'); ?>
                                    </span>
                                </div>

                                <div class="mask1"></div>
                                <a class="mask1vs1" href="<?php the_permalink(); ?>"><span class="openbox">+</span></a>
                            </div>
                        </div>
                    <?php endwhile; else: ?>
                    <?php ale_part('notfound')?>
                    <?php endif; ?>
                </div>
            </div>

        </div>
</section>
<?php get_footer(); ?>