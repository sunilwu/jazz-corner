<?php get_header(); ?>
    <section class="page menupagesection cf">
        <div class="wrapper">
            <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="http://localhost/delizioso/">Home</a></span>&nbsp; › &nbsp;<span class="current">Error 404</span></div>
            <div class="pagetitle">
                <h2 class="errortitle"><?php _e('Error 404','aletheme'); ?></h2>
            </div>

            <div class="menutypelist cf">
                <div class="custompage cf">
                    <div class="errorp1">
                        <?php _e('Error, Page not found','aletheme'); ?>
                    </div>
                    <p class="errorp2"><?php _e('Sorry, but the page you\'re looking for has not found. Try checking the URL for errors, then hit the refresh<br /> button on your browser.','aletheme'); ?></p>
                    <p class="errortitle"><a href="<?php echo home_url();?>" class="gohomebut bodyfont cusstyle17"><?php _e('Return to the homepage','aletheme'); ?></a></p>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>