<?php get_header(); ?>
<section class="page singleblog cf">
    <div class="wrapper">
        <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php echo home_url(); ?>"><?php _e('Home','aletheme'); ?></a></span>&nbsp; › &nbsp;<span class="current">Blog</span></div>
        <div class="cf"></div>
        <?php if(ale_get_option('blogpages')=='2c-l-fixed'){
            ale_part('blog-left-sidebar');
        } elseif(ale_get_option('blogpages')=='2c-r-fixed') {
            ale_part('blog-right-sidebar');
        } ?>
        <div class="content <?php if(ale_get_option('blogpages')=='1col-fixed') { echo "fullwidth"; } elseif(ale_get_option('blogpages')=='2c-l-fixed'){ echo "leftsidebaron"; } elseif(ale_get_option('blogpages')=='2c-r-fixed') { echo "rightsidebaron"; } ?>">
            <div class="blogtypetitle">
                <h2><?php _e('Blog',''); ?></h2>
            </div>
            <div id="post" class="blogarchive cf">
                <div class="boxarchive">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php ale_part('posthead');?>
                <?php ale_part('postpreview' );?>
                <?php ale_part('postfooter');?>
                <?php endwhile; else: ?>
                <?php ale_part('notfound')?>
                <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="cf"></div>
        <div class="paginationbox">
            <?php ale_page_links(); ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>