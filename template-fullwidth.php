<?php
/**
 * Template Name: Full Width
 */
get_header(); ?>
    <section class="page menupagesection cf">
        <div class="wrapper">
            <?php get_breadcrumbs(); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="pagetitle">
                <h2><?php the_title(); ?></h2>
            </div>

            <div class="menutypelist cf">
                <div class="custompage cf">
                    <?php the_content(); ?>
                </div>
                <?php endwhile; else: ?>
                    <?php ale_part('notfound')?>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>