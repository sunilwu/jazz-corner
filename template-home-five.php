<?php
/*
 * Template name: Home 5
 */

get_header(); ?>
    <div class="cf"></div>
    <section class="page cf">
        <div class="iconssection cf">
            <div class="wrapper cf">
                <div class="iconbox iconone">
                    <div class="imageboxicon" style="background: url(<?php echo ale_get_option('iconboxicon1'); ?>) 50% 50% no-repeat;"></div>
                    <div class="titleboxicon">
                        <?php echo ale_get_option('iconboxtit1'); ?>
                    </div>
                    <div class="descboxicon">
                        <?php echo ale_get_option('iconboxdesc1'); ?>
                    </div>
                    <div class="linkboxicon">
                        <a href="<?php echo ale_get_option('iconboxlink1'); ?>" class="button"><?php _e('Read more','aletheme'); ?></a>
                    </div>
                </div>
                <div class="iconbox icontwo">
                    <div class="imageboxicon" style="background: url(<?php echo ale_get_option('iconboxicon2'); ?>) 50% 50% no-repeat;"></div>
                    <div class="titleboxicon">
                        <?php echo ale_get_option('iconboxtit2'); ?>
                    </div>
                    <div class="descboxicon">
                        <?php echo ale_get_option('iconboxdesc2'); ?>
                    </div>
                    <div class="linkboxicon">
                        <a href="<?php echo ale_get_option('iconboxlink2'); ?>" class="button"><?php _e('Read more','aletheme'); ?></a>
                    </div>
                </div>
                <div class="iconbox iconthree">
                    <div class="imageboxicon" style="background: url(<?php echo ale_get_option('iconboxicon3'); ?>) 50% 50% no-repeat;"></div>
                    <div class="titleboxicon">
                        <?php echo ale_get_option('iconboxtit3'); ?>
                    </div>
                    <div class="descboxicon">
                        <?php echo ale_get_option('iconboxdesc3'); ?>
                    </div>
                    <div class="linkboxicon">
                        <a href="<?php echo ale_get_option('iconboxlink3'); ?>" class="button"><?php _e('Read more','aletheme'); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="cf"></div>
        <div class="abouthome cf" style="background: url('<?php echo ale_get_option('aboutboximg'); ?>') center no-repeat; background-size: cover;">
            <div class="blackbg cf">
                <div class="wrapper cf">
                    <div class="abouttitle">
                        <?php echo ale_get_option('aboutboxtit'); ?>
                    </div>
                    <div class="aboutdescr">
                        <?php echo ale_get_option('aboutboxdesc'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="reviews revcolor cf">
            <div class="wrapper">
                <div class="revtit"><?php echo ale_get_option('revtit'); ?></div>
                <div class="revitems">
                    <div class="itemrev">
                        <div class="leftimage">
                            <img src="<?php echo ale_get_option('revimage1') ?>" alt="<?php echo ale_get_option('revname1'); ?>" />
                        </div>
                        <div class="rightinfo">
                            <div class="titlere"><?php echo ale_get_option('revname1'); ?></div>
                            <div class="descre"><?php echo ale_get_option('revdesc1') ?></div>
                        </div>
                    </div>
                    <div class="itemrev">
                        <div class="leftimage">
                            <img src="<?php echo ale_get_option('revimage2') ?>" alt="<?php echo ale_get_option('revname2'); ?>" />
                        </div>
                        <div class="rightinfo">
                            <div class="titlere"><?php echo ale_get_option('revname2'); ?></div>
                            <div class="descre"><?php echo ale_get_option('revdesc2') ?></div>
                        </div>
                    </div>
                    <div class="itemrev">
                        <div class="leftimage">
                            <img src="<?php echo ale_get_option('revimage3') ?>" alt="<?php echo ale_get_option('revname3'); ?>" />
                        </div>
                        <div class="rightinfo">
                            <div class="titlere"><?php echo ale_get_option('revname3'); ?></div>
                            <div class="descre"><?php echo ale_get_option('revdesc3') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cf"></div>
        <div class="iconssectiontwo cf">
            <div class="wrapper homeblogsect cf">
                <div class="bloghometitle"><?php _e('Recent from Blog','aletheme'); ?></div>
                <div id="post" class="blogarchive cf">
                    <div class="boxarchive">
                        <?php
                        query_posts('&post_type=post&posts_per_page=6');
                        if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php ale_part('posthead');?>
                            <?php ale_part('postpreview' );?>
                            <?php ale_part('postfooter');?>
                        <?php endwhile; else: ?>
                            <?php ale_part('notfound')?>
                        <?php endif; wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>