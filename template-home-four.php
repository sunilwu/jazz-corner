<?php
/*
 * Template name: Home 4
 */

get_header(); ?>
    <div class="cf"></div>
    <section class="page cf">
        <div class="iconssection cf">
            <div class="wrapper cf">
                <div class="iconbox iconone">
                    <div class="imageboxicon" style="background: url(<?php echo ale_get_option('iconboxicon1'); ?>) 50% 50% no-repeat;"></div>
                    <div class="titleboxicon">
                        <?php echo ale_get_option('iconboxtit1'); ?>
                    </div>
                    <div class="descboxicon">
                        <?php echo ale_get_option('iconboxdesc1'); ?>
                    </div>
                    <div class="linkboxicon">
                        <a href="<?php echo ale_get_option('iconboxlink1'); ?>" class="button"><?php _e('Read more','aletheme'); ?></a>
                    </div>
                </div>
                <div class="iconbox icontwo">
                    <div class="imageboxicon" style="background: url(<?php echo ale_get_option('iconboxicon2'); ?>) 50% 50% no-repeat;"></div>
                    <div class="titleboxicon">
                        <?php echo ale_get_option('iconboxtit2'); ?>
                    </div>
                    <div class="descboxicon">
                        <?php echo ale_get_option('iconboxdesc2'); ?>
                    </div>
                    <div class="linkboxicon">
                        <a href="<?php echo ale_get_option('iconboxlink2'); ?>" class="button"><?php _e('Read more','aletheme'); ?></a>
                    </div>
                </div>
                <div class="iconbox iconthree">
                    <div class="imageboxicon" style="background: url(<?php echo ale_get_option('iconboxicon3'); ?>) 50% 50% no-repeat;"></div>
                    <div class="titleboxicon">
                        <?php echo ale_get_option('iconboxtit3'); ?>
                    </div>
                    <div class="descboxicon">
                        <?php echo ale_get_option('iconboxdesc3'); ?>
                    </div>
                    <div class="linkboxicon">
                        <a href="<?php echo ale_get_option('iconboxlink3'); ?>" class="button"><?php _e('Read more','aletheme'); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="cf"></div>
        <div class="abouthome cf" style="background: url('<?php echo ale_get_option('aboutboximg'); ?>') center no-repeat; background-size: cover;">
            <div class="blackbg cf">
                <div class="wrapper cf">
                    <div class="abouttitle">
                        <?php echo ale_get_option('aboutboxtit'); ?>
                    </div>
                    <div class="aboutdescr">
                        <?php echo ale_get_option('aboutboxdesc'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="reviews revcolor cf">
            <div class="wrapper">
                <div class="revtit"><?php echo _e('Latest events','aletheme'); ?></div>
                <div class="revitems">
                    <?php query_posts('&post_type=event&posts_per_page=3'); if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="itemrev">
                            <div class="leftimage">
                                <?php echo get_the_post_thumbnail($post->ID,'event-kva'); ?>
                            </div>
                            <div class="rightinfo">
                                <div class="titlere"><?php the_title(); ?></div>
                                <div class="descre"><?php echo ale_truncate(get_the_excerpt(),85); ?></div>
                            </div>
                        </div>
                    <?php endwhile; else: ?>
                        <?php ale_part('notfound')?>
                    <?php endif; wp_reset_query(); ?>
                </div>
            </div>
        </div>
        <div class="cf"></div>
        <div class="iconssectiontwo cf">
            <div class="wrapper cf">
                <div class="menutypelist gallerygrid homegally cf">
                    <div class="galleryfilterbox cf">
                        <ul id="filters" class="cf">
                            <li class="cf"><a href="#" class="active fil" data-filter="*"><?php _e('Show all', 'aletheme')?></a></li>
                            <?php $args = array(
                                'type'                     => 'gallery',
                                'child_of'                 => 0,
                                'parent'                   => '',
                                'orderby'                  => 'name',
                                'order'                    => 'ASC',
                                'hide_empty'               => 1,
                                'hierarchical'             => 1,
                                'exclude'                  => '',
                                'include'                  => '',
                                'number'                   => '',
                                'taxonomy'                 => 'gallery-category',
                                'pad_counts'               => false );

                            $categories = get_categories( $args );

                            foreach($categories as $cat){
                                echo '<li class="cf"><a href="#" class="fil" data-filter=".'.$cat->slug.'">'.$cat->name.'</a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                    <div id="post" class="galleryitems cf">
                        <?php query_posts('&post_type=gallery&posts_per_page=9'); if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div class="post galitem element <?php $terms = get_the_terms($post->id, 'gallery-category'); foreach($terms as $itcat) { echo $itcat->slug.' ';} ?>">
                                <div class="imagegally">
                                    <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'gallery-tumba'); ?></a>
                                    <div class="mask2">
                                        <div class="gallytitle"><a href="<?php the_permalink(); ?>"><?php echo ale_truncate(get_the_title(),20); ?></a></div>
                                        <div class="gallycat">
                                            <?php
                                            _e('Category: ','aletheme');
                                            $current_category = wp_get_post_terms($post->ID, 'gallery-category', array("fields" => "all"));
                                            if($current_category){
                                                foreach($current_category as $curcat){
                                                    echo $curcat->name.' ';
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div class="gallydate bodyfont">
                                            <?php echo get_the_date(); ?>
                                        </div>
                                    </div>
                                    <div class="mask0"><a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'gallery-tumba'); ?></a></div>
                                    <div class="countgal"><span>
                                    <?php
                                    $args = array(
                                        'post_type' => 'attachment',
                                        'numberposts' => -1,
                                        'post_status' => null,
                                        'order'				=> 'ASC',
                                        'orderby'			=> 'menu_order ID',
                                        'meta_query'		=> array(
                                            array(
                                                'key'		=> '_ale_hide_from_gallery',
                                                'value'		=> 0,
                                                'type'		=> 'DECIMAL',
                                            ),
                                        ),
                                        'post_parent' => $post->ID
                                    );
                                    $attachments = get_posts( $args );

                                    echo count($attachments); ?>
                                    <?php _e('Photos','aletheme'); ?>
                                    </span>
                                    </div>

                                    <div class="mask1"></div>
                                    <a class="mask1vs1" href="<?php the_permalink(); ?>"><span class="openbox">+</span></a>
                                </div>
                            </div>
                        <?php endwhile; else: ?>
                            <?php ale_part('notfound')?>
                        <?php endif; wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>