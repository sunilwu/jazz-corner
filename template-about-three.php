<?php
/**
 * Template Name: Template About 3
 */
get_header(); ?>
    <section class="page menupagesection cf">
        <div class="wrapper cf">
            <?php get_breadcrumbs(); ?>
            <div class="pagetitle pageabouttit galletit">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="cf"></div>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="abouttwoto">
                    <div class="leftpart">
                        <?php the_content(); ?>
                    </div>
                    <div class="rightpart">
                        <div class="siglepostvide">
                            <?php
                            if(ale_get_meta('videoaboutlink')){
                                echo wp_oembed_get(ale_get_meta('videoaboutlink'), array('width'=>500));
                            } else {
                                echo "Please, add the video link in the video field.";
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="cf"></div>
                <div class="aboutpageteam cf">
                    <div class="boxtitle">
                        <h3><?php echo ale_get_meta('meetteamtitle'); ?></h3>
                    </div>
                    <div class="teamphotos cf">
                        <?php if(ale_get_meta('firstphotoperson')){ ?>
                            <div class="teamitem">
                                <figure class="tubmapost">
                                    <div class="imagebox"><img src="<?php echo ale_get_meta('firstphotoperson'); ?>" /></div>
                                    <figcaption class="cf">
                                        <div class="name"><?php echo ale_get_meta('firstnameperson'); ?></div>
                                        <div class="profession"><?php echo ale_get_meta('firstprofperson'); ?></div>
                                    </figcaption>
                                </figure>
                            </div>
                        <?php } ?>
                        <?php if(ale_get_meta('secondphotoperson')){ ?>
                            <div class="teamitem">
                                <figure class="tubmapost">
                                    <div class="imagebox"><img src="<?php echo ale_get_meta('secondphotoperson'); ?>" /></div>
                                    <figcaption class="cf">
                                        <div class="name headerfont"><?php echo ale_get_meta('secondnameperson'); ?></div>
                                        <div class="profession"><?php echo ale_get_meta('secondprofperson'); ?></div>
                                    </figcaption>
                                </figure>
                            </div>
                        <?php } ?>
                        <?php if(ale_get_meta('thirdphotoperson')){ ?>
                            <div class="teamitem">
                                <figure class="tubmapost">
                                    <div class="imagebox"><img src="<?php echo ale_get_meta('thirdphotoperson'); ?>" /></div>
                                    <figcaption class="cf">
                                        <div class="name headerfont"><?php echo ale_get_meta('thirdnameperson'); ?></div>
                                        <div class="profession"><?php echo ale_get_meta('thirdprofperson'); ?></div>
                                    </figcaption>
                                </figure>
                            </div>
                        <?php } ?>
                        <?php if(ale_get_meta('fourthphotoperson')){ ?>
                            <div class="teamitem">
                                <figure class="tubmapost">
                                    <div class="imagebox"><img src="<?php echo ale_get_meta('fourthphotoperson'); ?>" /></div>
                                    <figcaption class="cf">
                                        <div class="name headerfont"><?php echo ale_get_meta('fourthnameperson'); ?></div>
                                        <div class="profession"><?php echo ale_get_meta('fourthprofperson'); ?></div>
                                    </figcaption>
                                </figure>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php endwhile; endif; ?>
        </div>
    </section>
<?php get_footer(); ?>