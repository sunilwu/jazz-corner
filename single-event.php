<?php get_header(); ?>
    <section class="page menupagesection cf">
        <div class="wrapper">
            <?php get_breadcrumbs(); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="pagetitle">
                    <h2><?php the_title(); ?></h2>
                </div>
                <div class="menutypelist cf menuitemsbottom">
                    <div class="bigimagebox cf">
                        <div class="menuslider">
                            <ul class="slides">
                                <?php
                                $args = array(
                                    'post_type' => 'attachment',
                                    'numberposts' => -1,
                                    'post_status' => null,
                                    'order'				=> 'ASC',
                                    'orderby'			=> 'menu_order ID',
                                    'meta_query'		=> array(
                                        array(
                                            'key'		=> '_ale_hide_from_gallery',
                                            'value'		=> 0,
                                            'type'		=> 'DECIMAL',
                                        ),
                                    ),
                                    'post_parent' => $post->ID
                                );
                                $attachments = get_posts( $args );
                                if ( $attachments ) {
                                    foreach ( $attachments as $attachment ) {
                                        echo "<li>".wp_get_attachment_image( $attachment->ID, 'menu-fullimage' )."</li>";
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="maskbox"></div>
                    </div>
                </div>
                <div class="descriptionmenusection cf">
                    <?php the_content(); ?>
                </div>

            <?php endwhile; else: ?>
                <?php ale_part('notfound')?>
            <?php endif; ?>
            <div class="cf"></div>
            <div class="recomitems">
                <h3><?php _e('Last Events','aletheme'); ?></h3>
            </div>
            <div class="eventtypelist cf">
                <?php wp_reset_query(); ?>
                <?php query_posts('&post_type=event&posts_per_page=2'); ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="menutypeitem">

                            <div class="whitebox cf">
                                <div class="menuimage">
                                    <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'event-kva'); ?></a>
                                    <div class="mask">
                                        <a href="<?php the_permalink(); ?>">
                                            <span>+</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="righteventpart">
                                    <div class="titile">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </div>
                                    <div class="date"><span><?php echo get_the_date(); ?></span></div>
                                    <div class="descr cf">
                                        <?php echo ale_truncate(get_the_excerpt(),200); ?>
                                    </div>
                                    <div class="menubottom cf">
                                        <div class="menulink fl">
                                            <a href="<?php the_permalink(); ?>" class="button redbutcolor">Read more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; else: ?>
                        <?php ale_part('notfound')?>
                    <?php endif; ?>
                </div>
        </div>
    </section>
<?php get_footer(); ?>