<?php get_header(); ?>
<section class="page singleblog cf">
    <div class="wrapper">
        <?php get_breadcrumbs(); ?>
        <div class="cf"></div>
        <?php if(ale_get_option('blogpages')=='2c-l-fixed'){
            ale_part('blog-left-sidebar');
        } elseif(ale_get_option('blogpages')=='2c-r-fixed') {
            ale_part('blog-right-sidebar');
        } ?>
        <div id="post" class="content <?php if(ale_get_option('blogpages')=='1col-fixed') { echo "fullwidth"; } elseif(ale_get_option('blogpages')=='2c-l-fixed'){ echo "leftsidebaron"; } elseif(ale_get_option('blogpages')=='2c-r-fixed') { echo "rightsidebaron"; } ?>">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <?php ale_part('posthead');?>
                    <div class="leftblogimage">

                        <?php if (has_post_format('gallery') == false and has_post_format('video') == false) : ?>
                            <div class="blogitemimage">
                                <?php echo get_the_post_thumbnail($post->ID,'post-preview'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (has_post_format('gallery')) : ?>
                            <div class="postslider ta">
                                <ul class="slides">
                                    <?php foreach(ale_get_attached_images() as $image):?>
                                        <li class="slimage"><?php echo wp_get_attachment_image($image->ID, 'post-preview')?></li>
                                    <?php endforeach;?>
                                </ul>
                            </div>
                        <?php elseif (has_post_format('video')): ?>
                            <div class="siglepostvide">
                                <?php
                                if(ale_get_meta('videopostlink')){
                                    echo wp_oembed_get(ale_get_meta('videopostlink'), array('width'=>400));
                                } else {
                                    echo "Please, add the video link in the video field.";
                                }
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="posttitle"><h2><?php the_title(); ?></h2></div>
                    <div class="metadatapost">
                        <span class="thedate"><?php echo get_the_date(); ?></span>
                        <span class="thecomnum"><?php echo get_comments_number($post->ID); ?></span>
                    </div>

                    <div class="topsingle">
                        <div class="divider cusstyle6"></div>

                        <div class="divider cusstyle7"></div>
                    </div>

                    <?php ale_part('postfull');?>

                    <div class="cf"></div>
                <?php ale_part('postfooter');?>
            <?php endwhile; else: ?>
                <?php ale_part('notfound')?>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>