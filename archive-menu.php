<?php get_header(); ?>
<section class="page menupagesection cf">
        <div class="wrapper">
            <?php get_breadcrumbs(); ?>
	</div>
        <div class="food-menu-slider">
	  <?php 
	  //echo do_shortcode("[metaslider id=835]"); 
	  echo do_shortcode("[metaslider id=828]"); 
	  ?>
	</div>  <!-- ENDS .food-menu-slider -->
	<div class="wrapper">
            <div class="pagetitle menutypetitle">
                <h2><?php _e('Our menu',''); ?></h2>
            </div>
            <div class="menutypelist cf">
	      <?php query_posts('post_type=menu&order=ASC'); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="menutypeitem">
                    <div class="menuimage">
                        <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'menu-grid'); ?></a>
                        <div class="mask">
                            <a href="<?php the_permalink(); ?>">
                                <span>+</span>
                            </a>
                        </div>
                    </div>
                    <div class="whitebox cf">
                        <div class="titile">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="descr">
                            <?php echo ale_truncate(get_the_excerpt(),200); ?>
                        </div>
                        <div class="menubottom cf">
                            <div class="menulink fl">
                                <a href="<?php the_permalink(); ?>" class="button redbutcolor">Read more</a>
                            </div>
                            <div class="pricemenu fr">
                                <?php echo ale_get_meta('itemcost'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; else: ?>
                <?php ale_part('notfound')?>
            <?php endif; ?>
            </div>
            <div class="paginationbox">
                <?php ale_page_links(); ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>
