<?php
/*
 * Template name: Home 3
 */

get_header(); ?>
    <div class="cf"></div>
    <section class="page cf">
        <div class="iconssection cf">
            <div class="wrapper homeblogsect cf">
                <div class="bloghometitle"><?php _e('Recent from Blog','aletheme'); ?></div>
                <div id="post" class="blogarchive cf">
                    <div class="boxarchive">
                        <?php
                        query_posts('&post_type=post&posts_per_page=6');
                        if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php ale_part('posthead');?>
                            <?php ale_part('postpreview' );?>
                            <?php ale_part('postfooter');?>
                        <?php endwhile; else: ?>
                            <?php ale_part('notfound')?>
                        <?php endif; wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="cf"></div>
        <div class="abouthome cf" style="background: url('<?php echo ale_get_option('aboutboximg'); ?>') center no-repeat; background-size: cover;">
            <div class="blackbg cf">
                <div class="wrapper cf">
                    <?php global $query_string;
                    query_posts($query_string);
                    if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="abouttitle">
                        <?php the_title(); ?>
                    </div>
                    <div class="aboutdescr">
                        <?php the_content(); ?>
                    </div>
                    <?php endwhile; else: ?>
                        <?php ale_part('notfound')?>
                    <?php endif; wp_reset_query(); ?>
                </div>
            </div>
        </div>
        <div class="reviews revcolor cf">
            <div class="wrapper">
                <div class="revtit"><?php echo _e('Clients reviews','aletheme'); ?></div>
                <div class="revitems">
                    <div class="itemrev">
                        <div class="leftimage">
                            <img src="<?php echo ale_get_option('revimage1') ?>" alt="<?php echo ale_get_option('revname1'); ?>" />
                        </div>
                        <div class="rightinfo">
                            <div class="titlere"><?php echo ale_get_option('revname1'); ?></div>
                            <div class="descre"><?php echo ale_get_option('revdesc1') ?></div>
                        </div>
                    </div>
                    <div class="itemrev">
                        <div class="leftimage">
                            <img src="<?php echo ale_get_option('revimage2') ?>" alt="<?php echo ale_get_option('revname2'); ?>" />
                        </div>
                        <div class="rightinfo">
                            <div class="titlere"><?php echo ale_get_option('revname2'); ?></div>
                            <div class="descre"><?php echo ale_get_option('revdesc2') ?></div>
                        </div>
                    </div>
                    <div class="itemrev">
                        <div class="leftimage">
                            <img src="<?php echo ale_get_option('revimage3') ?>" alt="<?php echo ale_get_option('revname3'); ?>" />
                        </div>
                        <div class="rightinfo">
                            <div class="titlere"><?php echo ale_get_option('revname3'); ?></div>
                            <div class="descre"><?php echo ale_get_option('revdesc3') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cf"></div>
        <div class="bottomtwoboxes cf">
            <div class="wrapper cf">
                <div class="fullmenubox">
                    <div class="ourmenubox cf">
                        <div class="titlebox cf">
                            <h2><?php _e('Our menu','aletheme'); ?></h2><ul class="custom-direction-nav"></ul>
                        </div>
                        <div class="menuitemslist cf">
                            <div class="menuslidetwo">
                                <ul class="slides">
                                    <?php query_posts('post_type=menu&posts_per_page=8');
                                    if (have_posts()) : while (have_posts()) : the_post(); ?>
                                        <li class="menufooditem">
                                            <div class="boxmenuitem cf">
                                                <div class="menuimage">
                                                    <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'menu-home'); ?></a>
                                                    <div class="mask">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <span>+</span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="whitebox cf">
                                                    <div class="titile">
                                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                    </div>
                                                    <div class="descr">
                                                        <?php echo ale_truncate(get_the_excerpt(),70); ?>
                                                    </div>
                                                    <div class="menubottom">
                                                        <div class="menulink fl">
                                                            <a href="<?php the_permalink(); ?>" class="button redbutcolor">Read more</a>
                                                        </div>
                                                        <div class="pricemenu fl">
                                                            <?php echo ale_get_meta('itemcost'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endwhile; endif; wp_reset_query(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
<?php get_footer(); ?>