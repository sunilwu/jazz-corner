// add your custom scripts here
// as the page loads, call these scripts
/*global Modernizr */
/*global ale */
/*global FB */
/*global gapi */
/*global twttr */
/*global pin_load */


jQuery(function($) {
    "use strict";

    var scrollstatus = $('html').data('scroll');



    $(window).load(function(){
        $('.preloader').fadeOut(1500).trigger('resize');

        $(window).scroll(function() {
            $('.imageboxicon').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+500) {
                    $(this).addClass("fadeIn");
                    $('.titleboxicon').addClass("slideDown");
                    $('.descboxicon').addClass("slideUp");
                }
            });

            $('.iconssection').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+500) {
                    $('.iconssection .homegally').addClass("slideUp");
                    $('.iconssection .homeblogsect').addClass("slideUp");
                }
            });

            $('.iconssectiontwo').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+500) {
                    $('.iconssectiontwo .homegally').addClass("slideUp");
                    $('.iconssectiontwo .homeblogsect').addClass("slideUp");
                }
            });

            $('.abouthome').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+400) {
                    $('.abouttitle').addClass('fadeIn');
                    $('.aboutdescr').addClass('expandUp');
                }
            });

            $('.reviews').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+450) {
                    $('.revtit').addClass('fadeIn');
                    $('.itemrev').addClass('expandUp');
                }
            });

            $('.bottomtwoboxes').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+500) {
                    $('.leftmenubox').addClass('slideRight');
                    $('.righteventbox').addClass('slideLeft');
                    $('.fullmenubox').addClass('slideUp');
                }
            });

        });

        if($(window).height() > 750) {
            $('.imageboxicon').each(function(){
                var imagePos = $(this).offset().top;

                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow+900) {
                    $(this).addClass("fadeIn");
                    $('.titleboxicon').addClass("slideDown");
                    $('.descboxicon').addClass("slideUp");
                }
            });
        }


        $('.day1').addClass("expandOpen");
        $('.day2').addClass("expandOpen");
        $('.day3').addClass("expandOpen");
        $('.day4').addClass("expandOpen");
        $('.day5').addClass("expandOpen");
        $('.day6').addClass("expandOpen");
        $('.day7').addClass("expandOpen");
    });

    $('.hide').click(function(){
        $('.topline').toggle(0);
    });

    $('.topslider .flexslider').flexslider(
        {
            animation:"fade",
            controlNav:false,
            slideshow: true,
            prevText:"",
            nextText:""
            //smoothHeight: true
        }
    );

    $('.menuslider').flexslider(
        {
            animation:"slide",
            controlNav:false,
            slideshow: true,
            prevText:"",
            nextText:""
            //smoothHeight: true
        }
    );

    $('.menuslide').flexslider(
        {
            animation:"slide",
            controlNav:false,
            animationLoop: true,
            itemWidth:230,
            directionNav:true,
            controlsContainer:".custom-direction-nav",
            minItems: 2,
            maxItems: 2
            //smoothHeight: true
        }
    );
    $('.menuslidetwo').flexslider(
        {
            animation:"slide",
            controlNav:false,
            animationLoop: true,
            itemWidth:230,
            directionNav:true,
            controlsContainer:".custom-direction-nav",
            minItems: 4,
            maxItems: 4
            //smoothHeight: true
        }
    );


    $('.postslider').flexslider({
        animation: "slide",
        controlNav: false,
        prevText: "",
        smoothHeight: true,
        nextText: ""
    });



        //Select Navigation on Mobile Devices
        $('select.mobilemenu').change(function(){
            var url = $(this).val();
            if (url) {
                window.location = url;
            }
            return false;
        });


    $(window).load(function(){
        if (jQuery.isFunction(jQuery.fn.isotope)) {
            // initialize Isotope
            $('.boxarchive').isotope({
                itemSelector : '.post'
            });
            $(window).smartresize(function(){
                $('.boxarchive').isotope({
                    itemSelector : '.post'
                });
            });
        }
    });

    if (jQuery.isFunction(jQuery.fn.isotope)) {
        //Filtering on Gallery pages

        // cache container
        var $container = $('.galleryitems');
        // initialize isotope
        $container.isotope({
            itemSelector : '.element'
        },function(){
            if(scrollstatus === 'scroll') {
                $("html").getNiceScroll().resize();
            }
        });

        // filter items when filter link is clicked
        $('#filters a.fil').live('click',function(){
            var itemtitle = $(this).text();
            var selector = $(this).attr('data-filter');
            $container.isotope({ filter: selector },function(){
                if(scrollstatus === 'scroll') {
                    $("html").getNiceScroll().resize();
                }
            });
            $('#filters a').removeClass('active');

            $(this).addClass('active');

            return false;
        });
        $(window).smartresize(function(){
            $container.isotope({},function(){
                if(scrollstatus === 'scroll') {
                    $("html").getNiceScroll().resize();
                }
            });
        });

    }

    if (jQuery.isFunction(jQuery.fn.scrollable)) {
        $('.sliderbox').scrollable({circular: true}).autoscroll({ autoplay: true });
    }

    $(window).load(function(){
        if(scrollstatus === 'scroll') {
            // Main window scroll
            $("html").niceScroll(
                {
                    cursorcolor: "#000000",
                    cursorborder:"0px",
                    cursorborderradius:"0px"
                }
            );
        }
    });
});

Modernizr.addTest('ipad', function () {
  "use strict";
  return !!navigator.userAgent.match(/iPad/i);
});

Modernizr.addTest('iphone', function () {
  "use strict";
  return !!navigator.userAgent.match(/iPhone/i);
});

Modernizr.addTest('ipod', function () {
  "use strict";
  return !!navigator.userAgent.match(/iPod/i);
});

Modernizr.addTest('appleios', function () {
  "use strict";
  return (Modernizr.ipad || Modernizr.ipod || Modernizr.iphone);
});

Modernizr.addTest('positionfixed', function () {
    "use strict";
    var test    = document.createElement('div'),
        control = test.cloneNode(false),
        fake = false,
        root = document.body || (function () {
            fake = true;
            return document.documentElement.appendChild(document.createElement('body'));
        }());

    var oldCssText = root.style.cssText;
    root.style.cssText = 'padding:0;margin:0';
    test.style.cssText = 'position:fixed;top:42px'; 
    root.appendChild(test);
    root.appendChild(control);
    
    var ret = test.offsetTop !== control.offsetTop;

    root.removeChild(test);
    root.removeChild(control);
    root.style.cssText = oldCssText;
    
    if (fake) {
        document.documentElement.removeChild(root);
    }
    
    /* Uh-oh. iOS would return a false positive here.
     * If it's about to return true, we'll explicitly test for known iOS User Agent strings.
     * "UA Sniffing is bad practice" you say. Agreeable, but sadly this feature has made it to
     * Modernizr's list of undectables, so we're reduced to having to use this. */
    return ret && !Modernizr.appleios;
});


// Modernizr.load loading the right scripts only if you need them
Modernizr.load([
	{
		// Let's see if we need to load selectivizr
		test : Modernizr.borderradius,
		// Modernizr.load loads selectivizr and Respond.js for IE6-8
		nope : [ale.template_dir + '/js/libs/selectivizr.min.js', ale.template_dir + '/js/libs/respond.min.js']
	},{
		test: Modernizr.touch,
		yep:ale.template_dir + '/css/touch.css'
	}
]);



jQuery(function($) {
    "use strict";

	var is_single = $('#post').length;
	var posts = $('article.post');
	var is_mobile = parseInt(ale.is_mobile, 10);
	
	var slider_settings = {
		animation: "slide",
		slideshow: false,
		controlNav: false
	};
	
    $(document).ajaxComplete(function(){
        try{
			if (!posts.length) {
				return;
			}
            FB.XFBML.parse(); 
            gapi.plusone.go();
            twttr.widgets.load();
			pin_load();
        }catch(ex){}
    });
	
    // open external links in new window
    $("a[rel$=external]").each(function(){
        $(this).attr('target', '_blank');
    });
	
	$.fn.init_posts = function() {
		var init_post = function(data) {
            // close other posts
            data.post.siblings('.open-post').find('a.toggle').trigger('click', {
                hide:true
            });
			
			var loading = data.post.find('span.loading');
			
			if (data.more.is(':empty')) {
				data.post.addClass('post-loading');
				loading.css('visibility', 'visible');
                data.more.load(ale.ajax_load_url, {
                    'action':'aletheme_load_post',
                    'id':data.post.data('post-id')
                }, function(){
                    loading.remove();
                    data.more.slideDown(400, function(){
                        data.post.addClass('open-post');
                        data.toggler.text('Close Post');
						$('.video', data.more).fitVids();
                        if (data.scroll) {
                            data.scroll.scrollTo('fast');
                        }
                    });
					init_comments(data.post);
                });
            } else {
                data.more.slideDown(400, function(){
                    data.post.addClass('open-post');
                    data.toggler.text('Close Post');
                    if (data.scroll) {
                        data.scroll.scrollTo('fast');
                    }
                });
            }
		};

		var load_post = function(e, _opts) {
			e.preventDefault();
            var data  = {
                toggler:$(this),
                scroll:false
            };
            var opts = $.extend({
                comments:false,
                hide:false,
                add_comment:false
            }, _opts);
            data.post = data.toggler.parents('article.post');
            data.more = data.post.find('.full');
			
            if (data.more.is(':visible')) {
                if (opts.hide === true) {
					// quick hide for multiple posts
                    data.more.hide();
                } else {
                    data.more.slideUp(400);
                }
                data.toggler.text('Open Post');
                data.post.removeClass('open-post');
            } else {
                if (typeof(e.originalEvent) !== 'undefined' ) {
                    data.scroll = data.post;
                }
                init_post(data);
            }
		};
		
		var init_comments = function(post) {
			var respond = $('section.respond', post);
			var respond_form = $('form', respond);
			var respond_form_error = $('p.error', respond_form);
			var respond_cancel = $('.cancel-comment-reply a', respond);
			var comments = $('section.comments', post);

			$('a.comment-reply-link', post).on('click', function(e){
				e.preventDefault();
				var comment = $(this).parents('li.comment');
				comment.find('>div').append(respond);
				respond_cancel.show();
				respond.find('input[name=comment_post_ID]').val(post.data('post-id'));
				respond.find('input[name=comment_parent]').val(comment.data('comment-id'));
				respond.find('input:first').focus();
			}).attr('onclick', '');

			respond_cancel.on('click', function(e){
				e.preventDefault();
				comments.after(respond);
				respond.find('input[name=comment_post_ID]').val(post.data('post-id'));
				respond.find('input[name=comment_parent]').val(0);
				$(this).hide();
			});


		};
		$(this).each(function(){
			var post = $(this);
			// init post galleries 
			$(window).load(function(){
				$('.preview .flexslider', post).flexslider(slider_settings);
			});
			// do not init ajax posts & comments for mobile
			if (!is_mobile) {
				// ajax posts enabled
                if (ale.ajax_posts) {
                    $('a.toggle', post).click(load_post);
					$('.video', post).fitVids();
					$('.preview figure a', post).click(function(e){
						e.preventDefault();
						$(this).parents('article.post').find('a.toggle').trigger('click');
					});
                }
			}
		});
		// init ajax comments on a single post if ajax comments are enabled
		if (is_single && parseInt(ale.ajax_comments, 10)) {
			init_comments(posts);	
		}
		// open single post on page
		if (parseInt(ale.ajax_open_single, 10) && !is_single && posts.length === 1) {
			posts.find('a.toggle').trigger('click');
		}
	};
	posts.init_posts();
	
	$.fn.init_gallery = function() {
		$(this).each(function(){
			var gallery = $(this);
			$(window).load(function(){
				$('.flexslider', gallery).flexslider(slider_settings);
			});
			
		});
	};
	$('#gallery').init_gallery();

    $.fn.init_archives = function()
    {
        $(this).each(function(){
            var archives = $(this);
            var year = $('#archives-active-year');
            var months = $('div.months div.year-months', archives);
            var arrows = $('a.up, a.down', archives);
            var activeMonth;
            var current, active;
            var animated = false;
			if (months.length === 1) {
				arrows.remove();
				activeMonth = months.filter(':first').addClass('year-active').show();
				year.text(activeMonth.attr('id').replace(/[^0-9]*/, ''));
				return;
			}
            arrows.click(function(e){
                e.preventDefault();
                if (animated) {
                    return;
                }
                var fn = $(this);
                animated = true;
                arrows.css('visibility', 'visible');
                var current = months.filter('.year-active');
                if (fn.hasClass('up')) {
                    active = current.prev();
                    if (!active.length) {
                        active = months.filter(':last');
                    }
                } else {
                    active = current.next();
                    if (!active.length) {
                        active = months.filter(':first');
                    }
                }
                current.removeClass('year-active').fadeOut(150, function(){
                    active.addClass('year-active').fadeIn(150, function(){
                        animated = false;
                    });
                    year.text(active.attr('id').replace(/[^0-9]*/, ''));

                    if (fn.hasClass('up')) {
                        if (!active.prev().length) {
                            arrows.filter('.up').css('visibility', 'hidden');
                        }
                    } else {
                        if (!active.next().length) {
                            arrows.filter('.down').css('visibility', 'hidden');
                        }
                    }
                });
            });
            activeMonth = months.filter(':first').addClass('year-active').show();
            year.text(activeMonth.attr('id').replace(/[^0-9]*/, ''));
            arrows.filter('.up').css('visibility', 'hidden');
        });
    };
    $('#archives .ale-archives').init_archives();
	
	
	
	

	
});

// HTML5 Fallbacks for older browsers
jQuery(function($) {
    "use strict";

    // check placeholder browser support
    if (!Modernizr.input.placeholder) {
        // set placeholder values
        $(this).find('[placeholder]').each(function() {
            $(this).val( $(this).attr('placeholder') );
        });
 
        // focus and blur of placeholders
        $('[placeholder]').focus(function() {
            if ($(this).val() === $(this).attr('placeholder')) {
                $(this).val('');
                $(this).removeClass('placeholder');
            }
        }).blur(function() {
            if ($(this).val() === '' || $(this).val() === $(this).attr('placeholder')) {
                $(this).val($(this).attr('placeholder'));
                $(this).addClass('placeholder');
            }
        });
 
        // remove placeholders on submit
        $('[placeholder]').closest('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                if ($(this).val() === $(this).attr('placeholder')) {
                    $(this).val('');
                }
            });
        });
    }
});

