<?php get_header(); global $query_string; query_posts($query_string.'&posts_per_page=10'); ?>
    <section class="page menupagesection cf">
        <div class="wrapper">
            <?php get_breadcrumbs(); ?>
            <div class="pagetitle eventtypetitle">
                <h2><?php _e('Our Events',''); ?></h2>
            </div>
            <div class="eventtypelist cf">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="menutypeitem">

                        <div class="whitebox cf">
                            <div class="menuimage">
                                <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'event-kva'); ?></a>
                                <div class="mask">
                                    <a href="<?php the_permalink(); ?>">
                                        <span>+</span>
                                    </a>
                                </div>
                            </div>
                            <div class="righteventpart">
                                <div class="titile">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </div>
                                <div class="date"><span><?php echo get_the_date(); ?></span></div>
                                <div class="descr cf">
                                    <?php echo ale_truncate(get_the_excerpt(),200); ?>
                                </div>
                                <div class="menubottom cf">
                                    <div class="menulink fl">
                                        <a href="<?php the_permalink(); ?>" class="button redbutcolor">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; else: ?>
                    <?php ale_part('notfound')?>
                <?php endif; ?>
            </div>
            <div class="paginationbox">
                <?php ale_page_links(); ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>