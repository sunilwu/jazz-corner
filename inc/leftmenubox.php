<?php
/***
*   This is a backup of an older piece of the frontpage.
*    This was a box that slide into position from the left.
*    It is being replaced by an event slideshow element
*
*/
?>

<div class="leftmenubox">
  <div class="ourmenubox cf">
    <div class="titlebox cf">
      <h2><?php _e('Our menu','aletheme'); ?></h2><ul class="custom-direction-nav"></ul>
    </div>
    <div class="menuitemslist cf">
      <div class="menuslide">
        <ul class="slides">
          <?php query_posts('post_type=menu&orderby=rand');
                if (have_posts()) : while (have_posts()) : the_post(); ?>
            <li class="menufooditem">
              <div class="boxmenuitem cf">
                <div class="menuimage">
                  <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'menu-home'); ?></a>
                  <div class="mask">
                    <a href="<?php the_permalink(); ?>">
                      <span>+</span>
                    </a>
                  </div>
                </div>
                <div class="whitebox cf">
                  <div class="titile">
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                  </div>
                  <div class="descr">
                    <?php echo ale_truncate(get_the_excerpt(),70); ?>
                  </div>
                  <div class="menubottom">
                    <div class="menulink fl">
                      <a href="<?php the_permalink(); ?>" class="button redbutcolor">Read more</a>
                    </div>
                    <div class="pricemenu fl">
                      <?php echo ale_get_meta('itemcost'); ?>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <?php endwhile; endif; wp_reset_query(); ?>
        </ul>
      </div>
    </div>
    <p>
      <a href="<?php  get_site_url() ?>/menu">View The Whole Menu</a>
    </p>
  </div>
</div>
