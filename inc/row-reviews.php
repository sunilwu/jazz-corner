    <div class="reviews revcolor cf">
        <div class="wrapper">
            <div class="revtit"><?php echo ale_get_option('revtit'); ?></div>
            <div class="revitems">
                <div class="itemrev">
                    <div class="leftimage">
                        <img src="<?php echo ale_get_option('revimage1') ?>" alt="<?php echo ale_get_option('revname1'); ?>" />
                    </div>
                    <div class="rightinfo">
                        <div class="titlere"><?php echo ale_get_option('revname1'); ?></div>
                        <div class="descre"><?php echo ale_get_option('revdesc1') ?></div>
                    </div>
                </div>
                <div class="itemrev">
                    <div class="leftimage">
                        <img src="<?php echo ale_get_option('revimage2') ?>" alt="<?php echo ale_get_option('revname2'); ?>" />
                    </div>
                    <div class="rightinfo">
                        <div class="titlere"><?php echo ale_get_option('revname2'); ?></div>
                        <div class="descre"><?php echo ale_get_option('revdesc2') ?></div>
                    </div>
                </div>
                <div class="itemrev">
                    <div class="leftimage">
                        <img src="<?php echo ale_get_option('revimage3') ?>" alt="<?php echo ale_get_option('revname3'); ?>" />
                    </div>
                    <div class="rightinfo">
                        <div class="titlere"><?php echo ale_get_option('revname3'); ?></div>
                        <div class="descre"><?php echo ale_get_option('revdesc3') ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
