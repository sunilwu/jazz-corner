<?php
/**
 * Get current theme options
 * 
 * @return array
 */
function aletheme_get_options() {
	$comments_style = array(
		'wp'  => 'Aletheme Comments',
		'fb'  => 'Facebook Comments',
		'dq'  => 'DISQUS',
		'lf'  => 'Livefyre',
		'off' => 'Disable All Comments',
	);

    $headerfont = array_merge(ale_get_safe_webfonts(), ale_get_google_webfonts());

    $background_defaults = array(
        'color' => '#ffffff',
        'image' => '',
        'repeat' => 'repeat',
        'position' => 'top center',
        'attachment'=>'scroll'
    );



	
	$imagepath =  ALETHEME_URL . '/assets/images/';
	
	$options = array();
		
	$options[] = array("name" => "Theme",
						"type" => "heading");

    $options[] = array( "name" => "Site Logo",
                        "desc" => "Upload or put the site logo link (Default logo size: 443-182px)",
                        "id" => "ale_sitelogo",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Logo Caption",
                        "desc" => "Type here your logo caption",
                        "id" => "ale_logocaption",
                        "std" => "Restaurant Wordpress Theme",
                        "type" => "text");

    $options[] = array( "name" => "Footer Logo",
                        "desc" => "Upload or put the footer logo link (Default logo size: 98-38px)",
                        "id" => "ale_footerlogo",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Mobile Header Logo",
                        "desc" => "Upload or put the mobile header logo link (Default logo size: 280-115px)",
                        "id" => "ale_mobsitelogo",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Site Color",
                        "desc" => "Change the color, the default is #f597b1",
                        "id" => "ale_site_color",
                        'std' => '#cf435a',
                        'type' => 'color');

    $options[] = array( "name" => "Gradient Color 1",
                        "desc" => "Change the color",
                        "id" => "ale_grad1_color",
                        'std' => '',
                        'type' => 'color');

    $options[] = array( "name" => "Gradient Color 2",
                        "desc" => "Change the color",
                        "id" => "ale_grad2_color",
                        'std' => '',
                        'type' => 'color');

    $options[] = array( "name" => "Border Gradient Color",
                        "desc" => "Change the color",
                        "id" => "ale_gradbor_color",
                        'std' => '',
                        'type' => 'color');

    $options[] = array( "name" => "Header image",
                        "desc" => "Upload or put the image link",
                        "id" => "ale_pageheader",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( 'name' => "Manage Background",
                        'desc' => "Select the background color, or upload a custom background image. Default background is the #ffffff color",
                        'id' => 'ale_background',
                        'std' => $background_defaults,
                        'type' => 'background');

    $options[] = array( "name" => "Uplaod a favicon icon",
                        "desc" => "Upload or put the link of your favicon icon",
                        "id" => "ale_favicon",
                        "std" => "",
                        "type" => "upload");


	$options[] = array( "name" => "Comments Style",
						"desc" => "Choose your comments style. If you want to use DISQUS comments please install and activate this plugin from <a href=\"" . admin_url('plugin-install.php?tab=search&type=term&s=Disqus+Comment+System&plugin-search-input=Search+Plugins') . "\">Wordpress Repository</a>.  If you want to use Livefyre Realtime Comments comments please install and activate this plugin from <a href=\"" . admin_url('plugin-install.php?tab=search&type=term&s=Livefyre+Realtime+Comments&plugin-search-input=Search+Plugins') . "\">Wordpress Repository</a>.",
						"id" => "ale_comments_style",
						"std" => "wp",
						"type" => "select",
						"options" => $comments_style);

	$options[] = array( "name" => "AJAX Comments",
						"desc" => "Use AJAX on comments posting (works only with Ellen Comments selected).",
						"id" => "ale_ajax_comments",
						"std" => "1",
						"type" => "checkbox");

	$options[] = array( "name" => "Social Sharing",
						"desc" => "Enable social sharing for posts.",
						"id" => "ale_social_sharing",
						"std" => "1",
						"type" => "checkbox");

	$options[] = array( "name" => "Copyrights",
						"desc" => "Your copyright message.",
						"id" => "ale_copyrights",
						"std" => "",
						"type" => "editor");


    $options[] = array( "name" => "Typography",
                        "type" => "heading");

    $options[] = array( "name" => "Select the Custom Font from Google Library",
                        "desc" => "The default Font is - Open Sans",
                        "id" => "ale_headerfont",
                        "std" => "Open+Sans",
                        "type" => "select",
                        "options" => $headerfont);

    $options[] = array( 'name' => "H1 Style",
                        'desc' => "Change the h1 style",
                        'id' => 'ale_h1sty',
                        'std' => array('size' => '28px','face' => 'Open+Sans','style' => 'normal','color' => '#2d2d2d'),
                        'type' => 'typography');

    $options[] = array( 'name' => "H2 Style",
                        'desc' => "Change the h2 style",
                        'id' => 'ale_h2sty',
                        'std' => array('size' => '26px','face' => 'Open+Sans','style' => 'normal','color' => '#2d2d2d'),
                        'type' => 'typography');

    $options[] = array( 'name' => "H3 Style",
                        'desc' => "Change the h3 style",
                        'id' => 'ale_h3sty',
                        'std' => array('size' => '24px','face' => 'Open+Sans','style' => 'normal','color' => '#2d2d2d'),
                        'type' => 'typography');

    $options[] = array( 'name' => "H4 Style",
                        'desc' => "Change the h4 style",
                        'id' => 'ale_h4sty',
                        'std' => array('size' => '22px','face' => 'Open+Sans','style' => 'normal','color' => '#2d2d2d'),
                        'type' => 'typography');

    $options[] = array( 'name' => "H5 Style",
                        'desc' => "Change the h5 style",
                        'id' => 'ale_h5sty',
                        'std' => array('size' => '20px','face' => 'Open+Sans','style' => 'normal','color' => '#2d2d2d'),
                        'type' => 'typography');

    $options[] = array( 'name' => "H6 Style",
                        'desc' => "Change the h6 style",
                        'id' => 'ale_h6sty',
                        'std' => array('size' => '18px','face' => 'Open+Sans','style' => 'normal','color' => '#2d2d2d'),
                        'type' => 'typography');

    $options[] = array( 'name' => "Body Style",
                        'desc' => "Change the body font style",
                        'id' => 'ale_bodystyle',
                        'std' => array('size' => '13px','face' => 'Open+Sans','style' => 'normal','color' => '#787878'),
                        'type' => 'typography');

    $options[] = array( "name" => "Home",
                        "type" => "heading");

    $options[] = array( "name" => "Phone number",
                        "desc" => "Your number",
                        "id" => "ale_phonenumber",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Address line one",
                        "desc" => "Your text",
                        "id" => "ale_addressone",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Address line two",
                        "desc" => "Your text",
                        "id" => "ale_addresstwo",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Contact email",
                        "desc" => "Your email",
                        "id" => "ale_contactemail",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Opening time subtitle",
                        "desc" => "Your text",
                        "id" => "ale_openingsubtitle",
                        "std" => "Opening hours",
                        "type" => "text");

    $options[] = array( "name" => "Week day 1",
                        "desc" => "Your text",
                        "id" => "ale_day1",
                        "std" => "Monday",
                        "type" => "text");

    $options[] = array( "name" => "Week day 2",
                        "desc" => "Your text",
                        "id" => "ale_day2",
                        "std" => "Tuesday",
                        "type" => "text");

    $options[] = array( "name" => "Week day 3",
                        "desc" => "Your text",
                        "id" => "ale_day3",
                        "std" => "Wednesday",
                        "type" => "text");

    $options[] = array( "name" => "Week day 4",
                        "desc" => "Your text",
                        "id" => "ale_day4",
                        "std" => "Thursday",
                        "type" => "text");

    $options[] = array( "name" => "Week day 5",
                        "desc" => "Your text",
                        "id" => "ale_day5",
                        "std" => "Friday",
                        "type" => "text");

    $options[] = array( "name" => "Week day 6",
                        "desc" => "Your text",
                        "id" => "ale_day6",
                        "std" => "Saturday",
                        "type" => "text");

    $options[] = array( "name" => "Week day 7",
                        "desc" => "Your text",
                        "id" => "ale_day7",
                        "std" => "Sunday",
                        "type" => "text");

    $options[] = array( "name" => "Week day 1 time",
                        "desc" => "Your text",
                        "id" => "ale_daytime1",
                        "std" => "12:00 AM - 12:00 PM",
                        "type" => "text");

    $options[] = array( "name" => "Week day 2 time",
                        "desc" => "Your text",
                        "id" => "ale_daytime2",
                        "std" => "12:00 AM - 12:00 PM",
                        "type" => "text");

    $options[] = array( "name" => "Week day 3 time",
                        "desc" => "Your text",
                        "id" => "ale_daytime3",
                        "std" => "Closed",
                        "type" => "text");

    $options[] = array( "name" => "Week day 4 time",
                        "desc" => "Your text",
                        "id" => "ale_daytime4",
                        "std" => "12:00 AM - 12:00 PM",
                        "type" => "text");

    $options[] = array( "name" => "Week day 5 time",
                        "desc" => "Your text",
                        "id" => "ale_daytime5",
                        "std" => "12:00 AM - 12:00 PM",
                        "type" => "text");

    $options[] = array( "name" => "Week day 6 time",
                        "desc" => "Your text",
                        "id" => "ale_daytime6",
                        "std" => "12:00 AM - 12:00 PM",
                        "type" => "text");

    $options[] = array( "name" => "Week day 7 time",
                        "desc" => "Your text",
                        "id" => "ale_daytime7",
                        "std" => "Closed",
                        "type" => "text");

    $options[] = array( "name" => "Icons box Title 1",
                        "desc" => "Your text",
                        "id" => "ale_iconboxtit1",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Icons box Title 2",
                        "desc" => "Your text",
                        "id" => "ale_iconboxtit2",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Icons box Title 3",
                        "desc" => "Your text",
                        "id" => "ale_iconboxtit3",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Icons box Icon 1",
                        "desc" => "Your icon (Recommended size - max - 80-80px )",
                        "id" => "ale_iconboxicon1",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Icons box Icon 2",
                        "desc" => "Your icon (Recommended size - max - 80-80px )",
                        "id" => "ale_iconboxicon2",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Icons box Icon 3",
                        "desc" => "Your icon (Recommended size - max - 80-80px )",
                        "id" => "ale_iconboxicon3",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Icons box Description 1",
                        "desc" => "Your description",
                        "id" => "ale_iconboxdesc1",
                        "std" => "",
                        "type" => "textarea");

    $options[] = array( "name" => "Icons box Description 2",
                        "desc" => "Your description",
                        "id" => "ale_iconboxdesc2",
                        "std" => "",
                        "type" => "textarea");

    $options[] = array( "name" => "Icons box Description 3",
                        "desc" => "Your description",
                        "id" => "ale_iconboxdesc3",
                        "std" => "",
                        "type" => "textarea");

    $options[] = array( "name" => "Icons box link 1",
                        "desc" => "Your link",
                        "id" => "ale_iconboxlink1",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Icons box link 2",
                        "desc" => "Your link",
                        "id" => "ale_iconboxlink2",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Icons box link 3",
                        "desc" => "Your link",
                        "id" => "ale_iconboxlink3",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "About box title",
                        "desc" => "Your text",
                        "id" => "ale_aboutboxtit",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "About box description",
                        "desc" => "Your text",
                        "id" => "ale_aboutboxdesc",
                        "std" => "",
                        "type" => "textarea");

    $options[] = array( "name" => "About box background",
                        "desc" => "Your image",
                        "id" => "ale_aboutboximg",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Reviews box title",
                        "desc" => "Your title",
                        "id" => "ale_revtit",
                        "std" => "Clients reviews",
                        "type" => "text");

    $options[] = array( "name" => "Reviews box name 1",
                        "desc" => "Your text",
                        "id" => "ale_revname1",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Reviews box name 2",
                        "desc" => "Your text",
                        "id" => "ale_revname2",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Reviews box name 3",
                        "desc" => "Your text",
                        "id" => "ale_revname3",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Reviews box image 1",
                        "desc" => "Your image",
                        "id" => "ale_revimage1",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Reviews box image 2",
                        "desc" => "Your image",
                        "id" => "ale_revimage2",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Reviews box image 3",
                        "desc" => "Your image",
                        "id" => "ale_revimage3",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Reviews box description 1",
                        "desc" => "Your text",
                        "id" => "ale_revdesc1",
                        "std" => "",
                        "type" => "textarea");

    $options[] = array( "name" => "Reviews box description 2",
                        "desc" => "Your text",
                        "id" => "ale_revdesc2",
                        "std" => "",
                        "type" => "textarea");

    $options[] = array( "name" => "Reviews box description 3",
                        "desc" => "Your text",
                        "id" => "ale_revdesc3",
                        "std" => "",
                        "type" => "textarea");


	$options[] = array( "name" => "Social",
						"type" => "heading");

    $options[] = array( "name" => "Twitter",
                        "desc" => "Your twitter username.",
                        "id" => "ale_twi",
                        "std" => "",
                        "type" => "text");
	$options[] = array( "name" => "Facebook",
						"desc" => "Your facebook profile URL.",
						"id" => "ale_fb",
						"std" => "",
						"type" => "text");
    $options[] = array( "name" => "Google+",
                        "desc" => "Your google+ profile URL.",
                        "id" => "ale_gog",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Show RSS",
                        "desc" => "Check if you want to show the RSS icon on your site",
                        "id" => "ale_rssicon",
                        "std" => "1",
                        "type" => "checkbox");

	
	$options[] = array( "name" => "Facebook Application ID",
						"desc" => "If you have Application ID you can connect the blog to your Facebook Profile and monitor statistics there.",
						"id" => "ale_fb_id",
						"std" => "",
						"type" => "text");
	
	$options[] = array( "name" => "Enable Open Graph",
						"desc" => "The <a href=\"http://www.ogp.me/\">Open Graph</a> protocol enables any web page to become a rich object in a social graph.",
						"id" => "ale_og_enabled",
						"std" => "",
						"type" => "checkbox");


	
	$options[] = array( "name" => "Advanced Settings",
						"type" => "heading");
	
	$options[] = array( "name" => "Google Analytics",
						"desc" => "Please insert your Google Analytics code here. Example: <strong>UA-22231623-1</strong>",
						"id" => "ale_ga",
						"std" => "",
						"type" => "text");
	
	$options[] = array( "name" => "Footer Code",
						"desc" => "If you have anything else to add in the footer - please add it here.",
						"id" => "ale_footer_info",
						"std" => "",
						"type" => "textarea");

    $options[] = array( "name" => "Custom CSS Styles",
                        "desc" => "You can add here your styles. ex. .boxclass { padding:10px; }",
                        "id" => "ale_customcsscode",
                        "std" => "",
                        "type" => "textarea");

    $options[] = array( "name" => "Home page Slider slug",
                        "desc" => "Insert the slider slug",
                        "id" => "ale_sliderslug",
                        "std" => "sneak-peek",
                        "type" => "text");

    $options[] = array( "name" => "Site PreLoader",
                        "desc" => "Check if you want to enable the site preloader. (We recommend to enable this option. All scripts will be loaded, and only after this, the site will appear and all options would working well.)",
                        "id" => "ale_preloader",
                        "std" => "1",
                        "type" => "checkbox");

    $options[] = array( "name" => "Enable or Disable custom scrollbarr",
                        "desc" => "Check if you want to show the custom scrollbar",
                        "id" => "ale_sitecustomscrollbar",
                        "std" => "0",
                        "type" => "checkbox");

    $options[] = array( "name" => "Show or hide black mask on Headers and Home Slider",
                        "desc" => "Check if you want to hide the black mask",
                        "id" => "ale_blackmask",
                        "std" => "0",
                        "type" => "checkbox");

    $options[] = array( "name" => "Site Currency",
                        "desc" => "Upload or put the image of your currency. Image size must be 10px-19px. It shows on Menu Archive page",
                        "id" => "ale_currencyimage",
                        "std" => "",
                        "type" => "upload");

    $options[] = array( "name" => "Site Currency name",
                        "desc" => "Insert the currency name. ex: Dollars",
                        "id" => "ale_currencyname",
                        "std" => "",
                        "type" => "text");

    $options[] = array( "name" => "Sidebars",
                        "type" => "heading");

    $options[] = array( 'name' => "Sidebar style on Blog pages",
                        'desc' => "Select sidebar style",
                        'id' => 'ale_blogpages',
                        'std' => '1col-fixed',
                        'type' => 'images',
                        'options' => array(
                            '1col-fixed' => $imagepath . '1col.png',
                            '2c-l-fixed' => $imagepath . '2cl.png',
                            '2c-r-fixed' => $imagepath . '2cr.png')
                        );
	
	return $options;
}

/**
 * Add custom scripts to Options Page
 */
function aletheme_options_custom_scripts() {
 ?>

<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#ale_commentongallery').click(function() {
        jQuery('#section-ale_gallerycomments_style').fadeToggle(400);
    });
    if (jQuery('#ale_commentongallery:checked').val() !== undefined) {
        jQuery('#section-ale_gallerycomments_style').show();
    }
});
</script>

<?php
}

/**
 * Add Metaboxes
 * @param array $meta_boxes
 * @return array 
 */
function aletheme_metaboxes($meta_boxes) {
	
	$meta_boxes = array();

    $prefix = "ale_";

    $meta_boxes[] = array(
        'id'         => 'post_page_metabox',
        'title'      => 'Post Data',
        'pages'      => array( 'post', ), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'Video Link',
                'desc' => 'You can put here the links from youtube.com, vimeo.com, blip.tv, dailymotion.com, flickr.com, smugmug.com, hulu.com, viddler.com, qik.com, revision3.com, photobucket.com, scribd.com, wordpress.tv, polldaddy.com, funnyordie.com, twitter.com, soundcloud.com, slideshare.net, instagram.com',
                'id'   => $prefix . 'videopostlink',
                'type'    => 'text',
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'menu_metabox',
        'title'      => 'Menu Item Options',
        'pages'      => array( 'menu', ), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
                'name' => 'Price',
                'desc' => 'Insert here your item cost.',
                'id'   => $prefix . 'itemcost',
                'type'    => 'text',
            ),
            array(
                'name' => 'Item Time Preparation',
                'desc' => 'Insert here your text.',
                'id'   => $prefix . 'minuter',
                'type'    => 'text',
            ),
            array(
                'name' => 'Item Portion number',
                'desc' => 'Insert here your text.',
                'id'   => $prefix . 'portion',
                'type'    => 'text',
            ),

            array(
                'name' => 'Item Recipe',
                'desc' => 'Insert here your text.',
                'id'   => $prefix . 'recipe',
                'type'    => 'textarea_code',
            ),
        )
    );


    $meta_boxes[] = array(
        'id'         => 'about_page_metabox',
        'title'      => 'About Skills Options',
        'pages'      => array( 'page', ), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'page-template', 'value' => array('template-about.php','template-about-two.php'), ), // Specific post templates to display this metabox
        'fields' => array(
            array(
                'name' => 'Skills box Title',
                'desc' => 'Insert your box title...',
                'id'   => $prefix . 'abouttwoskills',
                'type'    => 'text',
            ),
            array(
                'name' => 'Skill One Title',
                'desc' => 'Insert your first skill title...',
                'id'   => $prefix . 'firstskill',
                'type'    => 'text',
            ),
            array(
                'name' => 'Skill One Percent',
                'desc' => 'Insert your first skill procent... ex. 80',
                'id'   => $prefix . 'firstskillper',
                'type'    => 'text',
            ),
            array(
                'name' => 'Skill Two Title',
                'desc' => 'Insert your second skill title...',
                'id'   => $prefix . 'secondskill',
                'type'    => 'text',
            ),
            array(
                'name' => 'Skill Two Percent',
                'desc' => 'Insert your second skill procent... ex. 80',
                'id'   => $prefix . 'secondskillper',
                'type'    => 'text',
            ),
            array(
                'name' => 'Skill Three Title',
                'desc' => 'Insert your third skill title...',
                'id'   => $prefix . 'thirdskill',
                'type'    => 'text',
            ),
            array(
                'name' => 'Skill Three Percent',
                'desc' => 'Insert your third skill procent... ex. 80',
                'id'   => $prefix . 'thirdskillper',
                'type'    => 'text',
            ),
            array(
                'name' => 'Skill Four Title',
                'desc' => 'Insert your fourth skill title...',
                'id'   => $prefix . 'fourthskill',
                'type'    => 'text',
            ),
            array(
                'name' => 'Skill Four Percent',
                'desc' => 'Insert your fourth skill procent... ex. 80',
                'id'   => $prefix . 'fourthskillper',
                'type'    => 'text',
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'aboutteam_page_metabox',
        'title'      => 'About Team Options',
        'pages'      => array( 'page', ), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'page-template', 'value' => array('template-about.php','template-about-two.php','template-about-three.php'), ), // Specific post templates to display this metabox
        'fields' => array(
            array(
                'name' => 'Team box Title',
                'desc' => 'Insert your box title...',
                'id'   => $prefix . 'meetteamtitle',
                'type'    => 'text',
            ),
            array(
                'name' => 'First Photo',
                'desc' => 'Insert photo #1',
                'id'   => $prefix . 'firstphotoperson',
                'type'    => 'file',
            ),
            array(
                'name' => 'Second Photo',
                'desc' => 'Insert photo #2',
                'id'   => $prefix . 'secondphotoperson',
                'type'    => 'file',
            ),
            array(
                'name' => 'Third Photo',
                'desc' => 'Insert photo #3',
                'id'   => $prefix . 'thirdphotoperson',
                'type'    => 'file',
            ),
            array(
                'name' => 'Fourth Photo',
                'desc' => 'Insert photo #4',
                'id'   => $prefix . 'fourthphotoperson',
                'type'    => 'file',
            ),
            array(
                'name' => 'First Name',
                'desc' => 'Insert name #1',
                'id'   => $prefix . 'firstnameperson',
                'type'    => 'text',
            ),
            array(
                'name' => 'Second Name',
                'desc' => 'Insert name #2',
                'id'   => $prefix . 'secondnameperson',
                'type'    => 'text',
            ),
            array(
                'name' => 'Third Name',
                'desc' => 'Insert name #3',
                'id'   => $prefix . 'thirdnameperson',
                'type'    => 'text',
            ),
            array(
                'name' => 'Fourth Name',
                'desc' => 'Insert name #4',
                'id'   => $prefix . 'fourthnameperson',
                'type'    => 'text',
            ),
            array(
                'name' => 'First Prof',
                'desc' => 'Insert prof #1',
                'id'   => $prefix . 'firstprofperson',
                'type'    => 'text',
            ),
            array(
                'name' => 'Second Prof',
                'desc' => 'Insert prof #2',
                'id'   => $prefix . 'secondprofperson',
                'type'    => 'text',
            ),
            array(
                'name' => 'Third Prof',
                'desc' => 'Insert prof #3',
                'id'   => $prefix . 'thirdprofperson',
                'type'    => 'text',
            ),
            array(
                'name' => 'Fourth Prof',
                'desc' => 'Insert prof #4',
                'id'   => $prefix . 'fourthprofperson',
                'type'    => 'text',
            ),
        )
    );


    $meta_boxes[] = array(
        'id'         => 'contact_page_metabox',
        'title'      => 'Contact Map Options',
        'pages'      => array( 'page', ), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'page-template', 'value' => array('template-contact.php','template-contact-two.php','template-contact-three.php'), ), // Specific post templates to display this metabox
        'fields' => array(
            array(
                'name' => 'Google Map',
                'desc' => 'Insert the html code',
                'id'   => $prefix . 'gomap',
                'type' => 'textarea_code',
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'aboutthree_page_metabox',
        'title'      => 'About Page Video Options',
        'pages'      => array( 'page', ), // Post type
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
        'show_on'    => array( 'key' => 'page-template', 'value' => array('template-about-three.php'), ), // Specific post templates to display this metabox
        'fields' => array(
            array(
                'name' => 'Video link',
                'desc' => 'Insert the link',
                'id'   => $prefix . 'videoaboutlink',
                'type' => 'text',
            ),
        )
    );







	
	return $meta_boxes;
}

/**
 * Get image sizes for images
 * 
 * @return array
 */
function aletheme_get_images_sizes() {
	return array(

        'gallery' => array(
            array(
                'name'      => 'gallery-tumba',
                'width'     => 330,
                'height'    => 250,
                'crop'      => true,
            ),
            array(
                'name'      => 'gallery-slider',
                'width'     => 960,
                'height'    => 600,
                'crop'      => true,
            ),
        ),

        'page' => array(
            array(
                'name'      => 'page-slider',
                'width'     => 9999,
                'height'    => 400,
                'crop'      => false,
            ),
        ),

        'post' => array(
            array(
                'name'      => 'post-preview',
                'width'     => 400,
                'height'    => 250,
                'crop'      => true,
            ),
        ),

        'event' => array(
            array(
                'name'      => 'event-kva',
                'width'     => 250,
                'height'    => 250,
                'crop'      => true,
            ),
        ),

        'menu' => array(
            array(
                'name'      => 'menu-home',
                'width'     => 230,
                'height'    => 150,
                'crop'      => true,
            ),
            array(
                'name'      => 'menu-grid',
                'width'     => 330,
                'height'    => 220,
                'crop'      => true,
            ),
            array(
                'name'      => 'menu-fullimage',
                'width'     => 960,
                'height'    => 600,
                'crop'      => true,
            ),
        ),
    );
}

/**
 * Add post types that are used in the theme 
 * 
 * @return array
 */
function aletheme_get_post_types() {
	return array(
        'gallery' => array(
            'config' => array(
                'public' => true,
                'menu_position' => 20,
                'has_archive'   => true,
                'supports'=> array(
                    'title',
                    'comments',
                    'editor',
                    'thumbnail',
                    'page-attributes',
                ),
                'show_in_nav_menus'=> true,
            ),
            'singular' => 'Gallery',
            'multiple' => 'Galleries',
            'columns'    => array(
                'first_image',
            )
        ),

        'menu' => array(
            'config' => array(
                'public' => true,
                'menu_position' => 20,
                'has_archive'   => true,
                'supports'=> array(
                    'title',
                    'comments',
                    'editor',
                    'thumbnail',
                    'page-attributes',
                ),
                'show_in_nav_menus'=> true,
            ),
            'singular' => 'Menu',
            'multiple' => 'Menu',
            'columns'    => array(
                'first_image',
            )
        ),

        'event' => array(
            'config' => array(
                'public' => true,
                'menu_position' => 20,
                'has_archive'   => true,
                'supports'=> array(
                    'title',
                    'comments',
                    'editor',
                    'thumbnail',
                    'page-attributes',
                ),
                'show_in_nav_menus'=> true,
            ),
            'singular' => 'Event',
            'multiple' => 'Events',
            'columns'    => array(
                'first_image',
            )
        ),
    );
}

/**
 * Add taxonomies that are used in theme
 * 
 * @return array
 */
function aletheme_get_taxonomies() {
	return array(

        'gallery-category'    => array(
            'for'        => array('gallery'),
            'config'    => array(
                'sort'        => true,
                'args'        => array('orderby' => 'term_order'),
                'hierarchical' => true,
            ),
            'singular'    => 'Gallery Category',
            'multiple'    => 'Gallery Categories',
        ),

        'menu-category'    => array(
            'for'        => array('menu'),
            'config'    => array(
                'sort'        => true,
                'args'        => array('orderby' => 'term_order'),
                'hierarchical' => true,
            ),
            'singular'    => 'Menu Category',
            'multiple'    => 'Menu Categories',
        ),
    );
}

/**
 * Add post formats that are used in theme
 * 
 * @return array
 */
function aletheme_get_post_formats() {
	return array(
        'gallery','video'
    );
}

/**
 * Get sidebars list
 * 
 * @return array
 */
function aletheme_get_sidebars() {
	$sidebars = array();
	return $sidebars;
}

/**
 * Predefine custom sliders
 * @return array
 */
function aletheme_get_sliders() {
	return array(
		'sneak-peek' => array(
			'title'		=> 'Sneak Peek',
		),
	);
}

/**
 * Post types where metaboxes should show
 * 
 * @return array
 */
function aletheme_get_post_types_with_gallery() {
	return array('post','gallery','menu');
}

/**
 * Add custom fields for media attachments
 * @return array
 */
function aletheme_media_custom_fields() {
	return array();
}