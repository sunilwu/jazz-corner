<?php
/*
 * Template name: Home
 */

get_header(); ?>
<div class="cf"></div>
<section id="slider-and-events"   class="page cf">
  <?php // get_template_part("inc/row-reviews")  ?>

  <div class="cf"></div>
  <div class="bottomtwoboxes cf">
    <div class="wrapper cf">
      <div class="leftmenubox">
        <div class="eventspane">
          <iframe
            src="https://docs.google.com/presentation/d/17lWVtCZVKwWwySJ1hm4SC_D5-dS58oPOwnGdvm1p0Tk/embed?start=true&amp;loop=true&amp;delayms=3000"
frameborder="0"
width="417"
height="330"
allowfullscreen="true"
mozallowfullscreen="true"
webkitallowfullscreen="true"
style="display: block;  height: 250px ; width: 415px">
          </iframe>
        </div> <!-- ENDS eventspane -->

      </div> <!-- ENDS leftmenubox -->

      <div class="righteventbox">
        <?php if ( dynamic_sidebar('events_widget_slot') ) : else : endif; ?>
      </div> <!-- ENDS .righteventbox -->
    </div>
  </div>
</section> <!-- ENDS #slider-and-events -->

<?php get_template_part("inc/about");   ?>

<?php get_footer(); ?>
