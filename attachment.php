<?php get_header(); ?>
<section class="page singleblog cf">
    <div class="wrapper">
        <?php get_breadcrumbs(); ?>
        <div class="cf"></div>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="content <?php if(ale_get_option('blogpages')=='1col-fixed') { echo "fullwidth"; } elseif(ale_get_option('blogpages')=='2c-l-fixed'){ echo "leftsidebaron"; } elseif(ale_get_option('blogpages')=='2c-r-fixed') { echo "rightsidebaron"; } ?>">
            <div class="blogtypetitle">
                <h2><?php _e('Archives','aletheme'); ?></h2>
            </div>
            <div class="custompage cf">
                <?php the_content(); ?>
            </div>
            <?php endwhile; else: ?>
                <?php ale_part('notfound')?>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>