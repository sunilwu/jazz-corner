<?php get_header(); global $query_string; query_posts($query_string.'&post_type=gallery&posts_per_page=-1');
$current_category = wp_get_post_terms($post->ID, 'gallery-category', array("fields" => "all")); ?>
    <section class="page menupagesection cf">
        <div class="wrapper">
            <?php get_breadcrumbs(); ?>
            <div class="pagetitle gallytypetitle">
                <h2><?php _e('Category: ','');
                    if($current_category){
                        foreach($current_category as $curcat){
                            echo $curcat->name.' ';
                        }
                    }
                    ?></h2>
            </div>
            <div class="menutypelist gallerygrid cf">
                <div id="post" class="galleryitems cf">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <div class="post galitem element">
                            <div class="imagegally">
                                <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'gallery-tumba'); ?></a>
                                <div class="mask2">
                                    <div class="gallytitle"><a href="<?php the_permalink(); ?>"><?php echo ale_truncate(get_the_title(),20); ?></a></div>
                                    <div class="gallycat">
                                        <?php
                                        _e('Category: ','aletheme');

                                        if($current_category){
                                            foreach($current_category as $curcat){
                                                echo $curcat->name.' ';
                                            }
                                        }
                                        ?>
                                    </div>
                                    <div class="gallydate bodyfont">
                                        <?php echo get_the_date(); ?>
                                    </div>
                                </div>
                                <div class="mask0"><a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'gallery-tumba'); ?></a></div>
                                <div class="countgal"><span>
                                    <?php
                                    $args = array(
                                        'post_type' => 'attachment',
                                        'numberposts' => -1,
                                        'post_status' => null,
                                        'order'				=> 'ASC',
                                        'orderby'			=> 'menu_order ID',
                                        'meta_query'		=> array(
                                            array(
                                                'key'		=> '_ale_hide_from_gallery',
                                                'value'		=> 0,
                                                'type'		=> 'DECIMAL',
                                            ),
                                        ),
                                        'post_parent' => $post->ID
                                    );
                                    $attachments = get_posts( $args );

                                    echo count($attachments); ?>
                                    <?php _e('Photos','aletheme'); ?>
                                    </span>
                                </div>

                                <div class="mask1"></div>
                                <a class="mask1vs1" href="<?php the_permalink(); ?>"><span class="openbox">+</span></a>
                            </div>
                        </div>
                    <?php endwhile; else: ?>
                        <?php ale_part('notfound')?>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </section>
<?php get_footer(); ?>