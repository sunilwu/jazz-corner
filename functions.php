<?php
/****************************************************************
 * DO NOT DELETE
 ****************************************************************/
if ( get_stylesheet_directory() == get_template_directory() ) {
	define('ALETHEME_PATH', get_template_directory() . '/aletheme');
	define('ALETHEME_URL', get_template_directory_uri() . '/aletheme');
} else {
    define('ALETHEME_PATH', get_theme_root() . '/delizioso/aletheme');
    define('ALETHEME_URL', get_theme_root_uri() . '/delizioso/aletheme');
}

require_once ALETHEME_PATH . '/init.php';

load_theme_textdomain( 'aletheme', get_template_directory() . '/lang' );
$locale = get_locale();
$locale_file = get_template_directory() . "/lang/$locale.php";
if ( is_readable($locale_file) )
    require_once($locale_file);

/****************************************************************
 * You can add your functions here.
 * 
 * BE CAREFULL! Functions will dissapear after update.
 * If you want to add custom functions you should do manual
 * updates only.
 ****************************************************************/


/****************************************************************
 * 
 * Following are additions by Sunil Williams
 * sunil@sunil.co.nz
 * 
 ****************************************************************/

// create widget area

function events_slot_init() {

	register_sidebar( array(
		'name' => 'Events Widget Slot',
		'id' => 'events_widget_slot',
		'before_widget' => '',
		'after_widget' => ''
	) );
}
add_action( 'widgets_init', 'events_slot_init' );

function sunil_additional_scripts() {

	wp_register_style( 'sunil_stylesheet', 
						get_stylesheet_directory_uri() . "/css/sunil.css"
					);

	wp_enqueue_style( 'sunil_stylesheet', get_stylesheet_directory_uri() );
	
}
add_action( 'wp_enqueue_scripts', 'sunil_additional_scripts' );

/****************************************************************
 * 
 * ENDS additions by Sunil
 * 
 ****************************************************************/