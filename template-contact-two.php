<?php
/**
 * Template Name: Template Contact 2
 */
// send contact
if (isset($_POST['contact'])) {
    $error = ale_send_contact($_POST['contact']);
}
get_header();
?>
    <section class="page menupagesection cf">
        <div class="wrapper cf">
            <?php get_breadcrumbs(); ?>
            <div class="pagetitle pagecontacttit">
                <h2><?php the_title(); ?></h2>
            </div>
        </div>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="wrapper cf">
            <div class="contacttop contacttexttop cf">
                <div class="featuredcontact">
                    <?php echo get_the_post_thumbnail($post->ID,'full'); ?>
                </div>
                <?php the_content(); ?>
            </div>
        </div>
        <div class="wrapper cf">
            <div class="textblock cf">
                <div class="leftpart">
                    <div class="boxtitle"><h3>Contact us</h3></div>

                    <div class="formbox cf cusstyle8">
                        <form method="post" action="<?php the_permalink();?>">
                            <?php if (isset($_GET['success'])) : ?>
                                <p class="success"><?php _e('Your email was sent successfully. Thank you for your message!', 'aletheme')?></p>
                            <?php endif; ?>
                            <?php if (isset($error) && isset($error['msg'])) : ?>
                                <p class="error"><?php echo $error['msg']?></p>
                            <?php endif; ?>
                            <div class="responditemform">
                                <label for="contact-form-name" class="bodyfont"><?php _e('Name', 'aletheme')?><span>*</span></label>
                                <input type="text" name="contact[name]" value="<?php echo isset($_POST['contact']['name']) ? $_POST['contact']['name'] : ''?>" required="required" id="contact-form-name" />
                            </div>
                            <div class="responditemform">
                                <label for="contact-form-email" class="bodyfont"><?php _e('Email', 'aletheme')?><span>*</span></label>
                                <input type="email" name="contact[email]" value="<?php echo isset($_POST['contact']['email']) ? $_POST['contact']['email'] : ''?>" required="required" id="contact-form-email" />
                            </div>
                            <div class="responditemform">
                                <label for="contact-form-phone" class="bodyfont"><?php _e('Phone', 'aletheme')?><span>*</span></label>
                                <input type="text" name="contact[phone]" value="<?php echo isset($_POST['contact']['phone']) ? $_POST['contact']['phone'] : ''?>" required="required" id="contact-form-phone" />
                            </div>
                            <div class="responditemform">
                                <label for="contact-form-guest" class="bodyfont"><?php _e('Guest count', 'aletheme')?></label>
                                <input type="text" name="contact[guest]" value="<?php echo isset($_POST['contact']['guest']) ? $_POST['contact']['guest'] : ''?>" id="contact-form-guest" />
                            </div>
                            <div class="responditemform">
                                <label for="contact-form-date" class="bodyfont"><?php _e('Date and Time', 'aletheme')?></label>
                                <input type="text" name="contact[date]" value="<?php echo isset($_POST['contact']['date']) ? $_POST['contact']['date'] : ''?>" id="contact-form-date" />
                            </div>
                            <div class="cf"></div>
                            <div class="responditemform">
                                <label for="contact-form-message" class="bodyfont"><?php _e('Message', 'aletheme')?><span>*</span></label>
                                <textarea name="contact[message]" id="contact-form-message" cols="40" rows="5" required="required"><?php echo isset($_POST['contact']['message']) ? $_POST['contact']['message'] : ''?></textarea>
                            </div>
                            <div class="responditemform">
                                <input type="submit" class="submit button redbutcolor" value="<?php _e('Send', 'aletheme')?>" />

                            </div>
                            <?php wp_nonce_field() ?>
                        </form>
                    </div>
                </div>
                <div class="rightpart">
                    <div class="boxtitle"><h3>Map</h3></div>
                    <div class="contacttextbo mapbottomcont">
                        <?php echo ale_get_meta('gomap'); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile; else: ?>
            <?php ale_part('notfound')?>
        <?php endif; ?>
    </section>
<?php get_footer(); ?>