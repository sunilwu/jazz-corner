    </div>
    <footer id="footer-main" role="contentinfo">
        <div class="topfooter cf">
            <div class="wrapper cf">
                <?php if (ale_get_option('copyrights')) : ?>
                    <p class="copy"><?php echo ale_option('copyrights'); ?></p>
                <?php else: ?>
                    <p class="copy">&copy; <?php _e('Copyright 2013, Delizioso Restaurant Responsive WordPress Theme', 'aletheme')?></p>
                <?php endif; ?>
                <div class="rightfooterpart">
                    <div class="footerlogo">
                        <?php if(ale_get_option('footerlogo')){ ?>
                            <h1 class="custfootlogo">
                                <a href="<?php echo home_url(); ?>"><img src="<?php echo ale_get_option('footerlogo'); ?>" /></a>
                            </h1>
                        <?php } else { ?>
                            <h1 class="fuuterimage">
                                <a href="<?php echo home_url(); ?>"><?php echo bloginfo('title'); ?></a>
                            </h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottomfooter">
            <div class="wrapper cf">
                <div class="leftpartfooter">
                    <div class="contactpart">
                        <span class="onecont">
                            <?php _e('Contact','aletheme'); ?>:
                        </span>
                        <span class="twocont">
                            <span class="phonefooter"><?php echo ale_get_option('phonenumber'); ?></span> <br />
                            <?php echo ale_get_option('contactemail'); ?>
                        </span>
                        <span class="threecont">
                            <?php echo ale_get_option('addressone'); ?><br />
                            <?php echo ale_get_option('addresstwo'); ?>
                        </span>
                    </div>
                </div>
                <div class="rightpartfooter">
                    <nav id="bottommenu" class="bottommenu cf" role="navigation">
                        <?php
                        if ( has_nav_menu( 'footer_menu' ) ) {
                            wp_nav_menu(array(
                                'theme_location'=> 'footer_menu',
                                'menu'			=> 'Footer Menu',
                                'menu_class'	=> 'headermenu cf',
                                'walker'		=> new Aletheme_Nav_Walker(),
                                'container'		=> '',
                            ));
                        }
                        ?>
                    </nav>
                </div>
            </div>
        </div>
	</footer>
<?php wp_footer(); ?>

</body>
</html>