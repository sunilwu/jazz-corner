<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> <?php if(ale_get_option('sitecustomscrollbar') == 1) { echo 'style="overflow:hidden;" data-scroll="scroll" '; } else { echo 'data-scroll="hidescroll"';} ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php wp_title('|', true, 'right'); bloginfo('name'); ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
    <?php if(ale_get_option('preloader')=='1'){ ?>
        <div class="preloader">
            <div class="spinner"></div>
        </div>
    <?php } ?>
    <header id="mainheader" class="cf">
        <div class="topline cf">
            <div class="wrapper cf">
                <div class="headerleftdata">
                    <span class="phonenum"><?php echo ale_get_option('phonenumber'); ?></span>
                    <span class="adresstext"><?php echo ale_get_option('addresstwo'); ?></span>
                </div>
                <div class="socialprof cf">
                    <?php if(ale_get_option('fb')){ echo '<a href="'.ale_get_option('fb').'" rel="external" title="Like us on Facebook" class="socic fbicon" >Facebook</a>'; } ?>
                    <?php if(ale_get_option('twi')){ echo '<a href="http://twitter.com/#!/'.ale_get_option('twi').'" title="Follow us on Twitter" rel="external" class="socic twiicon" >Twitter</a>'; } ?>
                    <?php if(ale_get_option('gog')){ echo '<a href="'.ale_get_option('gog').'" rel="external" title="Trip Advisor" class="socic gogicon" 
>Google+</a>'; } ?>
                    <?php if(ale_get_option('rssicon')){?><a href="<?php echo home_url(); ?>/feed" rel="external" title="Our RSS" class="socic rssicon">RSS</a><?php } ?>
                </div>
            </div>
        </div>
        <div class="cf"></div>
        <div class="topslider cf">
            <div class="navsection colornavstyle cf">
              <div class="wrapper cf">
                    <div class="navigationbox">
                        <nav id="topmenu" class="topmenu cf" role="navigation">
			  
                          <div id="nav-header-logo">
                            <a href="<?php  echo home_url() ?>">
			      <img src="<?php echo get_template_directory_uri()  ?>/img/jazz-corner-logo.png" alt="jazz corner"/>
			    </a>
			  </div>
                            <?php
                            if ( has_nav_menu( 'header_menu' ) ) {
                                wp_nav_menu(array(
                                    'theme_location'=> 'header_menu',
                                    'menu'			=> 'Header Menu',
                                    'menu_class'	=> 'headermenu cf group',
                                    'walker'		=> new Aletheme_Nav_Walker(),
                                    'container'		=> ''
                                ));
                            }
                            ?>
                        </nav>
                        <nav id="mobilenav" class="topmenu cf" role="navigation">
                          <div class="mobile-logo">
			    <img src="<?php echo get_template_directory_uri()  ?>/img/jazz-corner-logo.png" alt="jazz corner"/>
</div>			  
                            <?php
                            if ( has_nav_menu( 'header_menu' ) ) {
                                wp_nav_menu(array(
                                    'theme_location'=> 'header_menu',
                                    'menu'   => 'Header Menu',
                                    'menu_class' => 'mobilemenu',
                                    'container'  => '',
                                    'items_wrap' => '<select id="%1$s" class="%2$s drop headerfont">%3$s</select>',
                                    'indent_string' => '&ndash;&nbsp;',
                                    'indent_after' => '',
                                    'walker' => new Aletheme_Dropdown_Nav_Walker(),
                                ));
                            } ?>
                        </nav>
                    </div>
		    <?php
		    /***
                    /* <div class="rightlinepart">
                    /*     <div class="hiddenbutton colorhidbut">
                    /*         <div class="hide"></div>
                      /*     </div>
		      */
                        ?>
                    </div>
                </div>
            </div>
            <?php if(is_front_page() or is_page_template('template-home-two.php') or is_page_template('template-home-three.php') or is_page_template('template-home-four.php') or is_page_template('template-home-five.php')){ ?>
	      <div class="homepage-slider-container">	
		<?php echo do_shortcode('[rev_slider homepage-header]')  ?>
	      </div>	    <!-- ENDS .homepage-slider-container -->
            <?php } else { ?>
                <div class="pageheder" style="background: url(<?php echo ale_get_option('pageheader'); ?>) 0% 00% no-repeat; background-size: cover;">
                    <div class="blackmask" <?php if(ale_get_option('blackmask')=="1"){ echo 'style="background:none;"';} ?>>
                        <div class="logobox">
                            <?php if(ale_get_option('sitelogo')){ ?>
                                <h1 class="customlogo">
                                    <a href="<?php echo home_url(); ?>"><img src="<?php echo ale_get_option('sitelogo'); ?>" /></a>
                                </h1>
                            <?php } else { ?>
                                <h1 class="image">
                                    <a href="<?php echo home_url(); ?>"><?php echo bloginfo('title'); ?></a>
                                </h1>
                            <?php } ?>
                            <span class="logocaption"><?php echo ale_get_option('logocaption'); ?></span>
                        </div>
                        <div class="mobilelogo">
                            <?php if(ale_get_option('mobsitelogo')){ ?>
                                <h1 class="customlogo">
                                    <a href="<?php echo home_url(); ?>"><img src="<?php echo ale_get_option('mobsitelogo'); ?>" /></a>
                                </h1>
                            <?php } else { ?>
                                <h1 class="image">
                                    <a href="<?php echo home_url(); ?>"><?php echo bloginfo('title'); ?></a>
                                </h1>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="bottomslider"></div>
        </div>
        <?php if(is_front_page() or is_page_template('template-home-two.php') or is_page_template('template-home-three.php') or is_page_template('template-home-four.php') or is_page_template('template-home-five.php')){ ?>
        <div class="mobileopening">
            <div class="timebox cf">
                <div class="dayline day1 cf">
                    <span class="dayname"><?php echo ale_get_option('day1'); ?></span>
                    <span class="daytime"><?php echo ale_get_option('daytime1'); ?></span>
                </div>
                <div class="dayline day2 cf">
                    <span class="dayname"><?php echo ale_get_option('day2'); ?></span>
                    <span class="daytime"><?php echo ale_get_option('daytime2'); ?></span>
                </div>
                <div class="dayline day3 cf">
                    <span class="dayname"><?php echo ale_get_option('day3'); ?></span>
                    <span class="daytime"><?php echo ale_get_option('daytime3'); ?></span>
                </div>
                <div class="dayline day4 cf">
                    <span class="dayname"><?php echo ale_get_option('day4'); ?></span>
                    <span class="daytime"><?php echo ale_get_option('daytime4'); ?></span>
                </div>
                <div class="dayline day5 cf">
                    <span class="dayname"><?php echo ale_get_option('day5'); ?></span>
                    <span class="daytime"><?php echo ale_get_option('daytime5'); ?></span>
                </div>
                <div class="dayline day6 cf">
                    <span class="dayname"><?php echo ale_get_option('day6'); ?></span>
                    <span class="daytime"><?php echo ale_get_option('daytime6'); ?></span>
                </div>
                <div class="dayline day7 cf">
                    <span class="dayname"><?php echo ale_get_option('day7'); ?></span>
                    <span class="daytime"><?php echo ale_get_option('daytime7'); ?></span>
                </div>
            </div>
        </div>
        <?php } ?>
    </header>
    <div id="content-main" role="main" class="cf">
