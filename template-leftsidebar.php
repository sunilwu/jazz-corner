<?php
/**
 * Template Name: Left Sidebar
 */
get_header(); ?>
<section class="page menupagesection cf">
    <div class="wrapper">
        <?php get_breadcrumbs(); ?>
        <div class="cf"></div>
        <?php ale_part('blog-left-sidebar'); ?>
        <div class="content leftsidebaron">
            <div class="pagetitle lson">
                <h2><?php the_title(); ?></h2>
            </div>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="custompage cf">
                    <?php the_content(); ?>
                </div>
            <?php endwhile; else: ?>
            <?php ale_part('notfound')?>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>