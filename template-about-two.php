<?php
/**
 * Template Name: Template About 2
 */
get_header(); ?>
    <section class="page menupagesection cf">
        <div class="wrapper cf">
            <?php get_breadcrumbs(); ?>
            <div class="pagetitle pageabouttit galletit">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="cf"></div>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="abouttwoto">
                <?php the_content(); ?>
            </div>

            <div class="aboutpagecontent">
                <div class="leftpart">
                    <div class="boxtitle"><h3><?php echo _e('Photos','aletheme'); ?></h3></div>
                    <div class="cusstyle12">
                        <div class="postslider sliderboxabouttwo cf">
                            <ul class="slides">
                                <?php
                                $args = array(
                                    'post_type' => 'attachment',
                                    'numberposts' => -1,
                                    'post_status' => null,
                                    'order'				=> 'ASC',
                                    'orderby'			=> 'menu_order ID',
                                    'meta_query'		=> array(
                                        array(
                                            'key'		=> '_ale_hide_from_gallery',
                                            'value'		=> 0,
                                            'type'		=> 'DECIMAL',
                                        ),
                                    ),
                                    'post_parent' => $post->ID
                                );
                                $attachments = get_posts( $args );
                                if ( $attachments ) {
                                    foreach ( $attachments as $attachment ) {
                                        echo "<li>".wp_get_attachment_image( $attachment->ID, 'full' )."</li>";
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="rightpart">
                    <div class="boxtitle"><h3><?php echo ale_get_meta('abouttwoskills'); ?></h3></div>
                    <ul class="skillul bodyfont">
                        <?php if(ale_get_meta('firstskill')){ echo '<li><span class="skititle">'.ale_get_meta('firstskill').'</span><div class="skillline"><div class="countline colornavstyle" style="width:'.ale_get_meta('firstskillper').'%">'.ale_get_meta('firstskillper').'%</div></div></li>'; } ?>
                        <?php if(ale_get_meta('secondskill')){ echo '<li><span class="skititle">'.ale_get_meta('secondskill').'</span><div class="skillline"><div class="countline colornavstyle" style="width:'.ale_get_meta('secondskillper').'%">'.ale_get_meta('secondskillper').'%</div></div></li>'; } ?>
                        <?php if(ale_get_meta('thirdskill')){ echo '<li><span class="skititle">'.ale_get_meta('thirdskill').'</span><div class="skillline"><div class="countline colornavstyle" style="width:'.ale_get_meta('thirdskillper').'%"> '.ale_get_meta('thirdskillper').'%</div></div></li>'; } ?>
                        <?php if(ale_get_meta('fourthskill')){ echo '<li><span class="skititle">'.ale_get_meta('fourthskill').'</span><div class="skillline"><div class="countline colornavstyle" style="width:'.ale_get_meta('fourthskillper').'%">'.ale_get_meta('fourthskillper').'%</div></div></li>'; } ?>
                    </ul>
                </div>
            </div>
            <div class="cf"></div>
            <div class="aboutpageteam cf">
                <div class="boxtitle">
                    <h3><?php echo ale_get_meta('meetteamtitle'); ?></h3>
                </div>
                <div class="teamphotos cf">
                    <?php if(ale_get_meta('firstphotoperson')){ ?>
                        <div class="teamitem">
                            <figure class="tubmapost">
                                <div class="imagebox"><img src="<?php echo ale_get_meta('firstphotoperson'); ?>" /></div>
                                <figcaption class="cf">
                                    <div class="name"><?php echo ale_get_meta('firstnameperson'); ?></div>
                                    <div class="profession"><?php echo ale_get_meta('firstprofperson'); ?></div>
                                </figcaption>
                            </figure>
                        </div>
                    <?php } ?>
                    <?php if(ale_get_meta('secondphotoperson')){ ?>
                        <div class="teamitem">
                            <figure class="tubmapost">
                                <div class="imagebox"><img src="<?php echo ale_get_meta('secondphotoperson'); ?>" /></div>
                                <figcaption class="cf">
                                    <div class="name headerfont"><?php echo ale_get_meta('secondnameperson'); ?></div>
                                    <div class="profession"><?php echo ale_get_meta('secondprofperson'); ?></div>
                                </figcaption>
                            </figure>
                        </div>
                    <?php } ?>
                    <?php if(ale_get_meta('thirdphotoperson')){ ?>
                        <div class="teamitem">
                            <figure class="tubmapost">
                                <div class="imagebox"><img src="<?php echo ale_get_meta('thirdphotoperson'); ?>" /></div>
                                <figcaption class="cf">
                                    <div class="name headerfont"><?php echo ale_get_meta('thirdnameperson'); ?></div>
                                    <div class="profession"><?php echo ale_get_meta('thirdprofperson'); ?></div>
                                </figcaption>
                            </figure>
                        </div>
                    <?php } ?>
                    <?php if(ale_get_meta('fourthphotoperson')){ ?>
                        <div class="teamitem">
                            <figure class="tubmapost">
                                <div class="imagebox"><img src="<?php echo ale_get_meta('fourthphotoperson'); ?>" /></div>
                                <figcaption class="cf">
                                    <div class="name headerfont"><?php echo ale_get_meta('fourthnameperson'); ?></div>
                                    <div class="profession"><?php echo ale_get_meta('fourthprofperson'); ?></div>
                                </figcaption>
                            </figure>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php endwhile; endif; ?>
        </div>
    </section>
<?php get_footer(); ?>