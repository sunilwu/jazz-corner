<?php get_header(); ?>
<section class="page menupagesection cf">
    <div class="wrapper cf">
        <?php get_breadcrumbs(); ?>
        <div class="pagetitle galletit">
            <h2><?php the_title(); ?></h2>
            <div class="subtitlegal">
                <span class="catgal"><?php _e('Posted in','aletheme'); ?>
                <?php
                $current_category = wp_get_post_terms($post->ID, 'gallery-category', array("fields" => "all"));
                if($current_category){
                    foreach($current_category as $curcat){
                        echo $curcat->name.' ';
                    }
                }
                ?></span>
                <span class="dategal"><?php echo get_the_date(); ?></span>
            </div>

        </div>
        <div class="galsliderarrows">
            <a class="prev browse left"></a>
            <a class="next browse right"></a>
        </div>
    </div>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="slidergallery">
            <div class="sliderbox cf">
                <div class="items">
                <?php
                $args = array(
                    'post_type' => 'attachment',
                    'numberposts' => -1,
                    'post_status' => null,
                    'order'				=> 'ASC',
                    'orderby'			=> 'menu_order ID',
                    'meta_query'		=> array(
                        array(
                            'key'		=> '_ale_hide_from_gallery',
                            'value'		=> 0,
                            'type'		=> 'DECIMAL',
                        ),
                    ),
                    'post_parent' => $post->ID
                );
                $attachments = get_posts( $args );
                if ( $attachments ) {
                    foreach ( $attachments as $attachment ) {
                        echo "<div>".wp_get_attachment_image( $attachment->ID, 'gallery-slider' )."</div>";
                    }
                }
                ?>
                </div>
            </div>


        </div>
    <div class="wrapper">
        <div class="textblock">
            <?php the_content(); ?>
        </div>
        <?php endwhile; else: ?>
            <?php ale_part('notfound')?>
        <?php endif; ?>

        <div class="recomitems">
            <h3><?php _e('Recent Galleries','aletheme'); ?></h3>
        </div>

        <?php wp_reset_query(); ?>
        <?php query_posts('&post_type=gallery&posts_per_page=3'); ?>
        <div class="gallerygrid onsinglegal">
            <div id="post" class="galleryitems cf">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="post galitem element <?php $terms = get_the_terms($post->id, 'gallery-category'); foreach($terms as $itcat) { echo $itcat->slug.' ';} ?>">
                    <div class="imagegally">
                        <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'gallery-tumba'); ?></a>
                        <div class="mask2">
                            <div class="gallytitle"><a href="<?php the_permalink(); ?>"><?php echo ale_truncate(get_the_title(),20); ?></a></div>
                            <div class="gallycat">
                                <?php
                                _e('Category: ','aletheme');
                                $current_category = wp_get_post_terms($post->ID, 'gallery-category', array("fields" => "all"));
                                if($current_category){
                                    foreach($current_category as $curcat){
                                        echo $curcat->name.' ';
                                    }
                                }
                                ?>
                            </div>
                            <div class="gallydate bodyfont">
                                <?php echo get_the_date(); ?>
                            </div>
                        </div>
                        <div class="mask0"><a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'gallery-tumba'); ?></a></div>
                        <div class="countgal"><span>
                                    <?php
                                    $args = array(
                                        'post_type' => 'attachment',
                                        'numberposts' => -1,
                                        'post_status' => null,
                                        'order'				=> 'ASC',
                                        'orderby'			=> 'menu_order ID',
                                        'meta_query'		=> array(
                                            array(
                                                'key'		=> '_ale_hide_from_gallery',
                                                'value'		=> 0,
                                                'type'		=> 'DECIMAL',
                                            ),
                                        ),
                                        'post_parent' => $post->ID
                                    );
                                    $attachments = get_posts( $args );

                                    echo count($attachments); ?>
                                    <?php _e('Photos','aletheme'); ?>
                                    </span>
                        </div>

                        <div class="mask1"></div>
                        <a class="mask1vs1" href="<?php the_permalink(); ?>"><span class="openbox">+</span></a>
                    </div>
                </div>
            <?php endwhile; else: ?>
                <?php ale_part('notfound')?>
            <?php endif; ?>
            </div>
        </div>

    </div>

</section>
<?php get_footer(); ?>