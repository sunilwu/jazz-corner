<?php get_header(); global $query_string; query_posts($query_string.'&post_type=menu') ?>
    <section class="page menupagesection cf">
        <div class="wrapper">
            <?php get_breadcrumbs(); ?>
            <div class="pagetitle menutypetitle">
                <h2><?php global $post;
                    $terms = get_the_terms($post->id, 'menu-category'); foreach ( $terms as $term ) {
                        echo $term->name;
                    } ?></h2>
            </div>
            <div class="menutypelist cf">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="menutypeitem">
                        <div class="menuimage">
                            <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID,'menu-grid'); ?></a>
                            <div class="mask">
                                <a href="<?php the_permalink(); ?>">
                                    <span>+</span>
                                </a>
                            </div>
                        </div>
                        <div class="whitebox cf">
                            <div class="titile">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </div>
                            <div class="descr">
                                <?php echo ale_truncate(get_the_excerpt(),200); ?>
                            </div>
                            <div class="menubottom cf">
                                <div class="menulink fl">
                                    <a href="<?php the_permalink(); ?>" class="button redbutcolor">Read more</a>
                                </div>
                                <div class="pricemenu fr">
                                    <?php echo ale_get_meta('itemcost'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; else: ?>
                    <?php ale_part('notfound')?>
                <?php endif; ?>
            </div>
            <div class="paginationbox">
                <?php ale_page_links(); ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>