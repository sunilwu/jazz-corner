<?php
$ale_background = ale_get_option('background');
$ale_headerfont = ale_get_option('headerfont');
$ale_font = ale_get_option('bodystyle');
$ale_h1 = ale_get_option('h1sty');
$ale_h2 = ale_get_option('h2sty');
$ale_h3 = ale_get_option('h3sty');
$ale_h4 = ale_get_option('h4sty');
$ale_h5 = ale_get_option('h5sty');
$ale_h6 = ale_get_option('h6sty');
?>
<?php if(ale_get_option('headerfont')){ echo "<link href='http://fonts.googleapis.com/css?family=".ale_get_option('headerfont').":300italic,400,600,300,700' rel='stylesheet' type='text/css'>"; } else { echo "<link href=\"http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic|Anton\" rel=\"stylesheet\" type=\"text/css\">"; } ?>
<style type='text/css'>
    body {
    <?php
    if($ale_background['color']){ echo "background-color:".$ale_background['color'].";"; } else { echo "background-color:#f5f5f5;"; }
    if($ale_background['image']){ echo "background-image: url(".$ale_background['image'].");"; };
    if($ale_background['repeat']){ echo "background-repeat:".$ale_background['repeat'].";"; };
    if($ale_background['position']){ echo "background-position:".$ale_background['position'].";"; };
    if($ale_background['attachment']){ echo "background-attachment:".$ale_background['attachment'].";"; };
    if($ale_font['size']){ echo "font-size:".$ale_font['size'].";"; };
    if($ale_font['style']){ echo "font-style:".$ale_font['style'].";"; };
    if($ale_font['color']){ echo "color:".$ale_font['color'].";"; };
    if($ale_font['face']){ $fontfamily =  str_replace ('+',' ',$ale_font['face']); echo "font-family:".$fontfamily.";"; };
    ?>
    }
    h1 {
    <?php
    if($ale_h1['size']){ echo "font-size:".$ale_h1['size'].";"; };
    if($ale_h1['style']){ echo "font-style:".$ale_h1['style'].";"; };
    if($ale_h1['color']){ echo "color:".$ale_h1['color'].";"; };
    if($ale_h1['face']){ $h1family =  str_replace ('+',' ',$ale_h1['face']); echo "font-family:".$h1family.";"; };
    ?>
    font-weight: 300;
    }
    h2 {
    <?php
    if($ale_h2['size']){ echo "font-size:".$ale_h2['size'].";"; };
    if($ale_h2['style']){ echo "font-style:".$ale_h2['style'].";"; };
    if($ale_h2['color']){ echo "color:".$ale_h2['color'].";"; };
    if($ale_h2['face']){ $h2family =  str_replace ('+',' ',$ale_h2['face']); echo "font-family:".$h2family.";"; };
    ?>
    font-weight: 300;
    }
    h3 {
    <?php
    if($ale_h3['size']){ echo "font-size:".$ale_h3['size'].";"; };
    if($ale_h3['style']){ echo "font-style:".$ale_h3['style'].";"; };
    if($ale_h3['color']){ echo "color:".$ale_h3['color'].";"; };
    if($ale_h3['face']){ $h3family =  str_replace ('+',' ',$ale_h3['face']); echo "font-family:".$h3family.";"; };
    ?>
        font-weight: 300;
    }
    h4 {
    <?php
    if($ale_h4['size']){ echo "font-size:".$ale_h4['size'].";"; };
    if($ale_h4['style']){ echo "font-style:".$ale_h4['style'].";"; };
    if($ale_h4['color']){ echo "color:".$ale_h4['color'].";"; };
    if($ale_h4['face']){ $h4family =  str_replace ('+',' ',$ale_h4['face']); echo "font-family:".$h4family.";"; };
    ?>
        font-weight: 300;
    }
    h5 {
    <?php
    if($ale_h5['size']){ echo "font-size:".$ale_h5['size'].";"; };
    if($ale_h5['style']){ echo "font-style:".$ale_h5['style'].";"; };
    if($ale_h5['color']){ echo "color:".$ale_h5['color'].";"; };
    if($ale_h5['face']){ $h5family =  str_replace ('+',' ',$ale_h5['face']); echo "font-family:".$h5family.";"; };
    ?>
        font-weight: 300;
    }
    h6 {
    <?php
    if($ale_h6['size']){ echo "font-size:".$ale_h6['size'].";"; };
    if($ale_h6['style']){ echo "font-style:".$ale_h6['style'].";"; };
    if($ale_h6['color']){ echo "color:".$ale_h6['color'].";"; };
    if($ale_h6['face']){ $h6family =  str_replace ('+',' ',$ale_h6['face']); echo "font-family:".$h6family.";"; };
    ?>
        font-weight: 300;
    }
    .customfont {
        font-family: <?php if($ale_headerfont) { echo str_replace ('+',' ',$ale_headerfont);} ?>;
    }
    .customcolor, a,
    .ale-team .testititle, .ale-testimonial .lefttestimonialpart {
        <?php if(ale_get_option('site_color')){ echo "color:".ale_get_option('site_color').";";} else { echo "color:#f597b1;"; } ?>
    }
    .custombg,.preloader,#mainheader .topslider .navsection .navigationbox #topmenu ul li ul li a:hover,
    .ale-team .socialbut .fbbut, .ale-team .socialbut .twibut,.ale-team .socialbut .gbut,
    .ale-divider span {
        <?php if(ale_get_option('site_color')){ echo "background-color:".ale_get_option('site_color').";";} else { echo "background-color:#f597b1;"; } ?>
    }
    input[type=text]:focus,
    input[type=email]:focus,
    input[type=url]:focus,
    input[type=search]:focus,
    input[type=password]:focus,
    textarea:focus {
        border-color: #cccccc;
    }

    input[type=text],
    input[type=email],
    input[type=url],
    input[type=search],
    input[type=password],
    textarea {
        font-family: <?php if($ale_headerfont) { echo str_replace ('+',' ',$ale_headerfont);} ?>;
        font-size: 12px;
    }
    button,
    input[type="button"],
    input[type="reset"],
    input[type="submit"] {
        <?php if(ale_get_option('site_color')){ echo "background-color:".ale_get_option('site_color').";";} else { echo "background-color:#f597b1;"; } ?>
        font-family: <?php if($ale_headerfont) { echo str_replace ('+',' ',$ale_headerfont);} ?>;
    }
    .tagcloud a {
        <?php if(ale_get_option('site_color')){ echo "background-color:".ale_get_option('site_color').";";} else { echo "background-color:#f597b1;"; } ?>
    }
    .jspDrag {
        <?php if(ale_get_option('site_color')){ echo "background-color:".ale_get_option('site_color').";";} else { echo "background-color:#f597b1;"; } ?>
    }

    .hiddenbutton .hide:hover {
        -moz-box-shadow:inset 1px 1px 2px 1px rgba(255, 255, 255, 0.2)!important;
        -webkit-box-shadow:inset 1px 1px 2px 1px rgba(255, 255, 255, 0.2)!important;
        box-shadow:inset 1px 1px 2px 1px rgba(255, 255, 255, 0.2)!important;
    }
    .colornavstyle {
        background: <?php if(ale_get_option('site_color')) { echo ale_get_option('site_color'); } else { echo "#B63345";} ?>;
        border-bottom:1px solid <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "#b12d47";} ?>;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='<?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#B63344";} ?>', endColorstr='<?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "#891D34";} ?>');
        background-image: linear-gradient(bottom, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "#891D34";} ?> 27%, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#B63344";} ?> 82%);
        background-image: -o-linear-gradient(bottom, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "#891D34";} ?> 27%, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#B63344";} ?> 82%);
        background-image: -moz-linear-gradient(bottom, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "#891D34";} ?> 27%, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#B63344";} ?> 82%);
        background-image: -webkit-linear-gradient(bottom, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "#891D34";} ?> 27%, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#B63344";} ?> 82%);
        background-image: -ms-linear-gradient(bottom, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "#891D34";} ?> 27%, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#B63344";} ?> 82%);

        background-image: -webkit-gradient(
            linear,
            left bottom,
            left top,
            color-stop(0.27, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "#891D34";} ?>),
            color-stop(0.82, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#B63344";} ?>)
        );
    }

    .colorhidbut {
        background: <?php if(ale_get_option('site_color')) { echo ale_get_option('site_color'); } else { echo "rgb(182, 51, 69)";} ?>;
        background: -moz-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(182, 51, 69)";} ?> 0%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(144, 32, 55)";} ?> 100%);
        background: -webkit-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(182, 51, 69)";} ?> 0%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(144, 32, 55)";} ?> 100%);
        background: -o-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(182, 51, 69)";} ?> 0%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(144, 32, 55)";} ?> 100%);
        background: -ms-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(182, 51, 69)";} ?> 0%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(144, 32, 55)";} ?> 100%);
        background: linear-gradient(180deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(182, 51, 69)";} ?> 0%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(144, 32, 55)";} ?> 100%);

        -webkit-box-shadow: 0px 1px 0px <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "rgba(198, 57, 85, 1)";} ?>;
        -moz-box-shadow:    0px 1px 0px <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "rgba(198, 57, 85, 1)";} ?>;
        box-shadow:         0px 1px 0px <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "rgba(198, 57, 85, 1)";} ?>;
    }
    .colorhidbut:hover {
        -webkit-box-shadow: 0px 1px 1px <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "rgba(126, 17, 38, 1)";} ?>;
        -moz-box-shadow:    0px 1px 1px <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "rgba(126, 17, 38, 1)";} ?>;
        box-shadow:         0px 1px 1px <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "rgba(126, 17, 38, 1)";} ?>;
    }
    #mainheader .topslider .rightlinepart .searchheader .searchinput {
        background-color: <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "#811a2c";} ?>;
        border:none;
        -webkit-box-shadow: 0px 1px 0px <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "rgba(177, 45, 71, 1)";} ?>;
        -moz-box-shadow:    0px 1px 0px <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "rgba(177, 45, 71, 1)";} ?>;
        box-shadow:         0px 1px 0px <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "rgba(177, 45, 71, 1)";} ?>;
        color:<?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#d34c67";} ?>;
        background-image: url(<?php echo get_template_directory_uri(); ?>/css/images/glass.png);
    }
    #mainheader .topslider .rightlinepart .searchheader .searchinput::-webkit-input-placeholder { /* WebKit browsers */
        color:<?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#d34c67";} ?>;
    }
    #mainheader .topslider .rightlinepart .searchheader .searchinput:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
        color:<?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#d34c67";} ?>;
    }
    #mainheader .topslider .rightlinepart .searchheader .searchinput::-moz-placeholder { /* Mozilla Firefox 19+ */
        color:<?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#d34c67";} ?>;
    }
    #mainheader .topslider .rightlinepart .searchheader .searchinput:-ms-input-placeholder { /* Internet Explorer 10+ */
        color:<?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "#d34c67";} ?>;
    }
    .revcolor {
        background: <?php if(ale_get_option('site_color')) { echo ale_get_option('site_color'); } else { echo "rgb(175, 47, 67)";} ?>;
        background: -moz-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(175, 47, 67)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(147, 34, 56)";} ?> 70%);
        background: -webkit-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(175, 47, 67)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(147, 34, 56)";} ?> 70%);
        background: -o-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(175, 47, 67)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(147, 34, 56)";} ?> 70%);
        background: -ms-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(175, 47, 67)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(147, 34, 56)";} ?> 70%);
        background: linear-gradient(180deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(175, 47, 67)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(147, 34, 56)";} ?> 70%);
        border-color: <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "#962739";} ?>;
    }
    .redbutcolor {
        background: <?php if(ale_get_option('site_color')) { echo ale_get_option('site_color'); } else { echo "rgb(223, 75, 97)";} ?>!important;
        background: -moz-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(223, 75, 97)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(173, 43, 71)";} ?> 70%)!important;
        background: -webkit-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(223, 75, 97)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(173, 43, 71)";} ?> 70%)!important;
        background: -o-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(223, 75, 97)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(173, 43, 71)";} ?> 70%)!important;
        background: -ms-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(223, 75, 97)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(173, 43, 71)";} ?> 70%)!important;
        background: linear-gradient(180deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgb(223, 75, 97)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgb(173, 43, 71)";} ?> 70%)!important;
        border-color: <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "#a4293f";} ?>!important;
        color:#ffffff!important;
        -moz-box-shadow:inset 0px 0px 0px 1px rgba(255,255,255,0.1)!important;
        -webkit-box-shadow:inset 0px 0px 0px 1px rgba(255,255,255,0.1)!important;
        box-shadow:inset 0px 0px 0px 1px rgba(255,255,255,0.1)!important;
        -moz-transition: all 300ms ease-in-out;
        -ms-transition: all 300ms ease-in-out;
        -o-transition: all 300ms ease-in-out;
        transition: all 300ms ease-in-out;
    }
    .redbutcolor:hover {
        background: <?php if(ale_get_option('site_color')) { echo ale_get_option('site_color'); } else { echo "rgb(223, 75, 97)";} ?>!important;
        background: -moz-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgba(223, 75, 97,0.9)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgba(173, 43, 71,0.9)";} ?> 70%)!important;
        background: -webkit-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgba(223, 75, 97,0.9)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgba(173, 43, 71,0.9)";} ?> 70%)!important;
        background: -o-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgba(223, 75, 97,0.9)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgba(173, 43, 71,0.9)";} ?> 70%)!important;
        background: -ms-linear-gradient(90deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgba(223, 75, 97,0.9)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgba(173, 43, 71,0.9)";} ?> 70%)!important;
        background: linear-gradient(180deg, <?php if(ale_get_option('grad1_color')) { echo ale_get_option('grad1_color'); } else { echo "rgba(223, 75, 97,0.9)";} ?> 30%, <?php if(ale_get_option('grad2_color')) { echo ale_get_option('grad2_color'); } else { echo "rgba(173, 43, 71,0.9)";} ?> 70%)!important;
        border-color: <?php if(ale_get_option('gradbor_color')) { echo ale_get_option('gradbor_color'); } else { echo "#a4293f";} ?>!important;
        color:#ffffff!important;
        -moz-box-shadow:inset 0px 0px 0px 1px rgba(255,255,255,0.1)!important;
        -webkit-box-shadow:inset 0px 0px 0px 1px rgba(255,255,255,0.1)!important;
        box-shadow:inset 0px 0px 0px 1px rgba(255,255,255,0.1)!important;
        -moz-transition: all 300ms ease-in-out;
        -ms-transition: all 300ms ease-in-out;
        -o-transition: all 300ms ease-in-out;
        transition: all 300ms ease-in-out;
    }

    .bottommenuslider {
        border-top:5px solid #000;
        <?php if(ale_get_option('site_color')){ echo "border-color:".ale_get_option('site_color').";";} else { echo "border-color:#f597b1;"; } ?>
    }
    <?php
        if(ale_get_option('currencyimage')){ ?>
        .page .menutypelist .menutypeitem .whitebox .menubottom .pricemenu { background: url(<?php echo ale_get_option('currencyimage'); ?>) left 50% no-repeat; }
        <?php }
    ?>
</style>