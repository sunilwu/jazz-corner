<header class="blogpreviewbox">
    <?php if (has_post_format('gallery') == false and has_post_format('video') == false) : ?>
        <div class="blogitemimage menuimage">
            <a href="<?php the_permalink(); ?>">
                <?php echo get_the_post_thumbnail($post->ID,'post-blog'); ?>
            </a>
            <div class="mask">
                <a href="<?php the_permalink(); ?>">
                    <span>+</span>
                </a>
            </div>
        </div>
    <?php endif; ?>
    <?php if (has_post_format('gallery')) : ?>
    <div class="postslider ta">
        <ul class="slides">
            <?php foreach(ale_get_attached_images() as $image):?>
                <li class="slimage"><a href="<?php the_permalink(); ?>"><?php echo wp_get_attachment_image($image->ID, 'post-blog')?></a></li>
            <?php endforeach;?>
        </ul>
    </div>
    <?php elseif (has_post_format('video')): ?>
        <div class="siglepostvide">
            <?php
            if(ale_get_meta('videopostlink')){
                echo wp_oembed_get(ale_get_meta('videopostlink'), array('width'=>1000));
            } else {
                echo "Please, add the video link in the video field.";
            }
            ?>
        </div>
    <?php endif; ?>
    <div class="textdata cf">
        <div class="dateev">
            <span class="dateday"><?php echo get_the_date( 'd', $post->ID ); ?></span><br />
            <span class="datemonth"><?php echo get_the_date( 'M', $post->ID ); ?></span><br />
            <span class="dateyear"><?php echo get_the_date( 'Y', $post->ID ); ?></span>
        </div>
        <div class="righttextdata">
            <div class="blogitemtitle">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </div>
            <div class="blogitemdate">
                <?php echo the_category(' ,'); ?>
            </div>
            <div class="blogitemtext">
                <?php the_excerpt(); ?>
            </div>
            <div class="blogitemmore">
                <a href="<?php the_permalink(); ?>" class="button redbutcolor"><?php _e('Read more','aletheme'); ?></a>
            </div>
        </div>
    </div>

</header>